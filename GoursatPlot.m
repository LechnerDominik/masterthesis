% t = tiledlayout(4,3);
% t.TileSpacing = 'compact';
% 
% t.Padding = 'compact';
% colormap parula
%% Dziuk plots
% t1=nexttile(1);
figure
load("Result_MCF_Dziuk/GoursatOcta_a=2h=0.15_solution_at_t0_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
title('Dziuk')
zlabel('Zeitpunkt 0');
axis equal
view(-69,17)
grid minor

print -depsc2 -painters Plots_Goursat/Dziuk_GoursatOcta_a=2h=0.15_solution_at_t0_BDF3_tau0.002.eps


% t2=nexttile(4);
load("Result_MCF_Dziuk/GoursatOcta_a=2h=0.15_solution_at_t0.1_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-69,17)
zlabel('Zeitpunkt 0.1');
grid minor
print -depsc2 -painters Plots_Goursat/Dziuk_GoursatOcta_a=2h=0.15_solution_at_t0.1_BDF3_tau0.002.eps

% t3=nexttile(7);
%subplot(4,3,7)
load("Result_MCF_Dziuk/GoursatOcta_a=2h=0.15_solution_at_t0.2_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-69,17)
zlabel('Zeitpunkt 0.2');
grid minor

print -depsc2 -painters Plots_Goursat/Dziuk_GoursatOcta_a=2h=0.15_solution_at_t0.2_BDF3_tau0.002.eps
% t4= nexttile(10);
% subplot(4,3,10)
load("Result_MCF_Dziuk/GoursatOcta_a=2h=0.15_solution_at_t0.4_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-69,17)
zlabel('Zeitpunkt 0.4');
grid minor
print -depsc2 -painters Plots_Goursat/Dziuk_GoursatOcta_a=2h=0.15_solution_at_t0.4_BDF3_tau0.002.eps

%% Dziuk ALE plots
% t1=nexttile(1);
figure
load("Result_MCF_Dziuk_ALE/GoursatOcta_a=2h=0.15_solution_at_t0_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
title('Dziuk ALE')
% zlabel('Zeitpunkt 0');
axis equal
view(-69,17)
grid minor

print -depsc2 -painters Plots_Goursat/Dziuk_ALE_GoursatOcta_a=2h=0.15_solution_at_t0_BDF3_tau0.002.eps

% t2=nexttile(4);
load("Result_MCF_Dziuk_ALE/GoursatOcta_a=2h=0.15_solution_at_t0.1_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-69,17)
% zlabel('Zeitpunkt 0.1');
grid minor
print -depsc2 -painters Plots_Goursat/Dziuk_ALE_GoursatOcta_a=2h=0.15_solution_at_t0.1_BDF3_tau0.002.eps

% t3=nexttile(7);
%subplot(4,3,7)
load("Result_MCF_Dziuk_ALE/GoursatOcta_a=2h=0.15_solution_at_t0.2_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-69,17)
% zlabel('Zeitpunkt 0.2');
grid minor
print -depsc2 -painters Plots_Goursat/Dziuk_ALE_GoursatOcta_a=2h=0.15_solution_at_t0.2_BDF3_tau0.002.eps

% t4= nexttile(10);
% subplot(4,3,10)
load("Result_MCF_Dziuk_ALE/GoursatOcta_a=2h=0.15_solution_at_t0.4_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-69,17)
% zlabel('Zeitpunkt 0.4');
grid minor
print -depsc2 -painters Plots_Goursat/Dziuk_ALE_GoursatOcta_a=2h=0.15_solution_at_t0.4_BDF3_tau0.002.eps



%% Lubich plot
% nexttile(2)

% subplot(4,3,2)
load("Result_MCF_Lubich/GoursatOcta_a=2h=0.15_solution_at_t0_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
title('Kov�cs et al')
axis equal
view(-69,17)
grid minor
print -depsc2 -painters Plots_Goursat/Kovacs_GoursatOcta_a=2h=0.15_solution_at_t0_BDF3_tau0.002.eps

% nexttile(5)
% subplot(4,3,5)
load("Result_MCF_Lubich/GoursatOcta_a=2h=0.15_solution_at_t0.1_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-69,17)
grid minor

print -depsc2 -painters Plots_Goursat/Kovacs_GoursatOcta_a=2h=0.15_solution_at_t0.1_BDF3_tau0.002.eps

% nexttile(8)
% subplot(4,3,8)
load("Result_MCF_Lubich/GoursatOcta_a=2h=0.15_solution_at_t0.2_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-69,17)
grid minor

print -depsc2 -painters Plots_Goursat/Kovacs_GoursatOcta_a=2h=0.15_solution_at_t0.2_BDF3_tau0.002.eps



% nexttile(11)
% subplot(4,3,11)
load("Result_MCF_Lubich/GoursatOcta_a=2h=0.15_solution_at_t0.4_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-69,17)
grid minor

print -depsc2 -painters Plots_Goursat/Kovacs_GoursatOcta_a=2h=0.15_solution_at_t0.4_BDF3_tau0.002.eps


%% Lubich ALE
% nexttile(3)
% subplot(4,3,3)
load("Result_MCF_Lubich_ALE/GoursatOcta_a=2h=0.15_solution_at_t0_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
title('Kov�cs et al. ALE')
axis equal
view(-69,17)
grid minor

print -depsc2 -painters Plots_Goursat/Kovacs_ALE_GoursatOcta_a=2h=0.15_solution_at_t0_BDF3_tau0.002.eps


% nexttile(6)
% subplot(4,3,6)
load("Result_MCF_Lubich_ALE/GoursatOcta_a=2h=0.15_solution_at_t0.1_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-69,17)
grid minor
print -depsc2 -painters Plots_Goursat/Kovacs_ALE_GoursatOcta_a=2h=0.15_solution_at_t0.1_BDF3_tau0.002.eps


% nexttile(9)
% subplot(4,3,9)
load("Result_MCF_Lubich_ALE/GoursatOcta_a=2h=0.15_solution_at_t0.2_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-69,17)
grid minor
print -depsc2 -painters Plots_Goursat/Kovacs_ALE_GoursatOcta_a=2h=0.15_solution_at_t0.2_BDF3_tau0.002.eps


% nexttile(12)
% subplot(4,3,12)
load("Result_MCF_Lubich_ALE/GoursatOcta_a=2h=0.15_solution_at_t0.4_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-69,17)
grid minor
print -depsc2 -painters Plots_Goursat/Kovacs_ALE_GoursatOcta_a=2h=0.15_solution_at_t0.4_BDF3_tau0.002.eps

