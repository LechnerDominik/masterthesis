function [nodes,elements,Nu,H,surfacename] = surfacemesh(mode,h,param)
addpath('distmesh');
% param=1 Ellipsoid with parameter a,b,c given by p(:,1).^2/a^2+p(:,2).^2/b^2+p(:,3).^2/c^2-1;
% param=2 Dumbbell given by p(:,1).^2+p(:,2).^2+(2*p(:,3).^2.*(p(:,3).^2-199/200))-0.04;
% param=3 Goursat Cube with parameter a given by x^4+y^4+z^4-a^2*(x^2+y^2+z^2)=0
% param=4 Goursat Octahedron with parameter a given by 8*(x^2*y^2+y^2*z^2+z^2*x^2)+a^2*(x^2+y^2+z^2)-a^4=0
%
%  h is approx an edgelength of a triangulation given by distmesh

%H, Nu were calculated by symscalc
% 2* bei H entsteht durch minimal andere Notation f�r die mittlere Kr�mmung

%Ellipsoid
if mode==1
    a=param.a;    b=param.b;    c=param.c;
    
    fd=@(p) p(:,1).^2/a^2+p(:,2).^2/b^2+p(:,3).^2/c^2-1;
    [nodes,elements]=distmeshsurface(fd,@huniform,h,[-a-1,-b-1,-c-1; a+1,b+1,c+1]);
    Nu=zeros(length(nodes),3);
    H=zeros(length(nodes),1);
    %trisurf(elements,nodes(:,1),nodes(:,2),nodes(:,3))
    
    for j=1:length(nodes)
        H(j)=2*(-1)*((8*nodes(j,1)^2)/a^6 - ((4*nodes(j,1)^2)/a^4 + (4*nodes(j,2)^2)/b^4 + (4*nodes(j,3)^2)/c^4)*(2/a^2 + 2/b^2 + 2/c^2) + (8*nodes(j,2)^2)/b^6 + (8*nodes(j,3)^2)/c^6)/(16*(nodes(j,1)^2/a^4 + nodes(j,2)^2/b^4 + nodes(j,3)^2/c^4)^(3/2));
        Nu(j,:)=[ nodes(j,1)/(a^2*(nodes(j,1)^2/a^4 + nodes(j,2)^2/b^4 + nodes(j,3)^2/c^4)^(1/2)), nodes(j,2)/(b^2*(nodes(j,1)^2/a^4 + nodes(j,2)^2/b^4 + nodes(j,3)^2/c^4)^(1/2)), nodes(j,3)/(c^2*(nodes(j,1)^2/a^4 + nodes(j,2)^2/b^4 + nodes(j,3)^2/c^4)^(1/2))];
    end

    surfacename=strcat('Ellipsoid_','a=',num2str(a),'b=',num2str(b),'c=',num2str(c),'h=',num2str(h));
elseif mode ==2 %Dumbbell
    fd=@(p) p(:,1).^2+p(:,2).^2+(2*p(:,3).^2.*(p(:,3).^2-199/200))-0.04;
    [nodes,elements]=distmeshsurface(fd,@huniform,h,3*[-1,-1,-1;1,1,1]);
    Nu=zeros(length(nodes),3);
    H=zeros(length(nodes),1);
    
    for j=1:length(nodes)
        H(j) =-2*((24*nodes(j,3)^2 - 199/50)*(4*nodes(j,3)*(nodes(j,3)^2 - 199/200) + 4*nodes(j,3)^3)^2 - (24*nodes(j,3)^2 + 1/50)*((4*nodes(j,3)*(nodes(j,3)^2 - 199/200) + 4*nodes(j,3)^3)^2 + 4*nodes(j,1)^2 + 4*nodes(j,2)^2) + 8*nodes(j,1)^2 + 8*nodes(j,2)^2)/(16*((4*nodes(j,3)*(nodes(j,3)^2 - 199/200) + 4*nodes(j,3)^3)^2/4 + nodes(j,1)^2 + nodes(j,2)^2)^(3/2));
        Nu(j,:) =[ nodes(j,1)/((4*nodes(j,3)*(nodes(j,3)^2 - 199/200) + 4*nodes(j,3)^3)^2/4 + nodes(j,1)^2 + nodes(j,2)^2)^(1/2), nodes(j,2)/((4*nodes(j,3)*(nodes(j,3)^2 - 199/200) + 4*nodes(j,3)^3)^2/4 + nodes(j,1)^2 + nodes(j,2)^2)^(1/2), (4*nodes(j,3)*(nodes(j,3)^2 - 199/200) + 4*nodes(j,3)^3)/(2*((4*nodes(j,3)*(nodes(j,3)^2 - 199/200) + 4*nodes(j,3)^3)^2/4 + nodes(j,1)^2 + nodes(j,2)^2)^(1/2))];
    end
        surfacename=strcat('Dumbbell_','h=',num2str(h));

    
elseif mode == 3 %Goursat Cube
    %x^4+y^4+z^4-a^2*(x^2+y^2+z^2)=0
    a=param.a;
    fd=@(p) (p(:,1).^4+p(:,2).^4+p(:,3).^4-a^2*(p(:,1).^2+p(:,2).^2+p(:,3).^2));
    [nodes,elements]=distmeshsurface(fd,@huniform,h,[-3,-3,-3;3,3,3]);
    Nu=zeros(length(nodes),3);
    H=zeros(length(nodes),1);
    
    for j=1:length(nodes)
        H(j) =-2*-(((2.*a.^2.*nodes(j,1) - 4.*nodes(j,1).^3).^2 + (2.*a.^2.*nodes(j,2) - 4.*nodes(j,2).^3).^2 + (2.*a.^2.*nodes(j,3) - 4.*nodes(j,3).^3).^2).*(- 6.*a.^2 + 12.*nodes(j,1).^2 + 12.*nodes(j,2).^2 + 12.*nodes(j,3).^2) + (2.*a.^2.*nodes(j,1) - 4.*nodes(j,1).^3).^2.*(2.*a.^2 - 12.*nodes(j,1).^2) + (2.*a.^2.*nodes(j,2) - 4.*nodes(j,2).^3).^2.*(2.*a.^2 - 12.*nodes(j,2).^2) + (2.*a.^2.*nodes(j,3) - 4.*nodes(j,3).^3).^2.*(2.*a.^2 - 12.*nodes(j,3).^2))./(16.*((2.*a.^2.*nodes(j,1) - 4.*nodes(j,1).^3).^2./4 + (2.*a.^2.*nodes(j,2) - 4.*nodes(j,2).^3).^2./4 + (2.*a.^2.*nodes(j,3) - 4.*nodes(j,3).^3).^2./4).^(3./2));
        Nu(j,:) =[ -(2.*a.^2.*nodes(j,1) - 4.*nodes(j,1).^3)./(2.*((2.*a.^2.*nodes(j,1) - 4.*nodes(j,1).^3).^2./4 + (2.*a.^2.*nodes(j,2) - 4.*nodes(j,2).^3).^2./4 + (2.*a.^2.*nodes(j,3) - 4.*nodes(j,3).^3).^2./4).^(1./2)), -(2.*a.^2.*nodes(j,2) - 4.*nodes(j,2).^3)./(2.*((2.*a.^2.*nodes(j,1) - 4.*nodes(j,1).^3).^2./4 + (2.*a.^2.*nodes(j,2) - 4.*nodes(j,2).^3).^2./4 + (2.*a.^2.*nodes(j,3) - 4.*nodes(j,3).^3).^2./4).^(1./2)), -(2.*a.^2.*nodes(j,3) - 4.*nodes(j,3).^3)./(2.*((2.*a.^2.*nodes(j,1) - 4.*nodes(j,1).^3).^2./4 + (2.*a.^2.*nodes(j,2) - 4.*nodes(j,2).^3).^2./4 + (2.*a.^2.*nodes(j,3) - 4.*nodes(j,3).^3).^2./4).^(1./2))];
    end
        surfacename=strcat('GoursatCube_','a=',num2str(a),'h=',num2str(h));

elseif mode == 4 %Goursat Octahedron
    %8*(x^2*y^2+y^2*z^2+z^2*x^2)+a^2*(x^2+y^2+z^2)-a^4=0
    a=param.a;
    fd=@(p) 8*(p(:,1).^2.*p(:,2).^2+p(:,2).^2.*p(:,3).^2+p(:,3).^2.*p(:,1).^2)+a^2*(p(:,1).^2+p(:,2).^2+p(:,3).^2)-a^4;
    [nodes,elements]=distmeshsurface(fd,@huniform,h,[-3,-3,-3;3,3,3]);
      Nu=zeros(length(nodes),3);
    H=zeros(length(nodes),1);
    
    for j=1:length(nodes)
        H(j)=-2*((2.*nodes(j,1).*a.^2 + 16.*nodes(j,1).*nodes(j,2).^2 + 16.*nodes(j,1).*nodes(j,3).^2).*((2.*nodes(j,1).*a.^2 + 16.*nodes(j,1).*nodes(j,2).^2 + 16.*nodes(j,1).*nodes(j,3).^2).*(2.*a.^2 + 16.*nodes(j,2).^2 + 16.*nodes(j,3).^2) + 32.*nodes(j,1).*nodes(j,2).*(2.*nodes(j,2).*a.^2 + 16.*nodes(j,2).*nodes(j,1).^2 + 16.*nodes(j,2).*nodes(j,3).^2) + 32.*nodes(j,1).*nodes(j,3).*(2.*nodes(j,3).*a.^2 + 16.*nodes(j,3).*nodes(j,1).^2 + 16.*nodes(j,3).*nodes(j,2).^2)) - ((2.*nodes(j,1).*a.^2 + 16.*nodes(j,1).*nodes(j,2).^2 + 16.*nodes(j,1).*nodes(j,3).^2).^2 + (2.*nodes(j,2).*a.^2 + 16.*nodes(j,2).*nodes(j,1).^2 + 16.*nodes(j,2).*nodes(j,3).^2).^2 + (2.*nodes(j,3).*a.^2 + 16.*nodes(j,3).*nodes(j,1).^2 + 16.*nodes(j,3).*nodes(j,2).^2).^2).*(6.*a.^2 + 32.*nodes(j,1).^2 + 32.*nodes(j,2).^2 + 32.*nodes(j,3).^2) + (2.*nodes(j,2).*a.^2 + 16.*nodes(j,2).*nodes(j,1).^2 + 16.*nodes(j,2).*nodes(j,3).^2).*((2.*nodes(j,2).*a.^2 + 16.*nodes(j,2).*nodes(j,1).^2 + 16.*nodes(j,2).*nodes(j,3).^2).*(2.*a.^2 + 16.*nodes(j,1).^2 + 16.*nodes(j,3).^2) + 32.*nodes(j,1).*nodes(j,2).*(2.*nodes(j,1).*a.^2 + 16.*nodes(j,1).*nodes(j,2).^2 + 16.*nodes(j,1).*nodes(j,3).^2) + 32.*nodes(j,2).*nodes(j,3).*(2.*nodes(j,3).*a.^2 + 16.*nodes(j,3).*nodes(j,1).^2 + 16.*nodes(j,3).*nodes(j,2).^2)) + (2.*nodes(j,3).*a.^2 + 16.*nodes(j,3).*nodes(j,1).^2 + 16.*nodes(j,3).*nodes(j,2).^2).*((2.*nodes(j,3).*a.^2 + 16.*nodes(j,3).*nodes(j,1).^2 + 16.*nodes(j,3).*nodes(j,2).^2).*(2.*a.^2 + 16.*nodes(j,1).^2 + 16.*nodes(j,2).^2) + 32.*nodes(j,1).*nodes(j,3).*(2.*nodes(j,1).*a.^2 + 16.*nodes(j,1).*nodes(j,2).^2 + 16.*nodes(j,1).*nodes(j,3).^2) + 32.*nodes(j,2).*nodes(j,3).*(2.*nodes(j,2).*a.^2 + 16.*nodes(j,2).*nodes(j,1).^2 + 16.*nodes(j,2).*nodes(j,3).^2)))./(16.*((2.*nodes(j,1).*a.^2 + 16.*nodes(j,1).*nodes(j,2).^2 + 16.*nodes(j,1).*nodes(j,3).^2).^2./4 + (2.*nodes(j,2).*a.^2 + 16.*nodes(j,2).*nodes(j,1).^2 + 16.*nodes(j,2).*nodes(j,3).^2).^2./4 + (2.*nodes(j,3).*a.^2 + 16.*nodes(j,3).*nodes(j,1).^2 + 16.*nodes(j,3).*nodes(j,2).^2).^2./4).^(3./2));
        Nu(j,:) =[ (2.*nodes(j,1).*a.^2 + 16.*nodes(j,1).*nodes(j,2).^2 + 16.*nodes(j,1).*nodes(j,3).^2)./(2.*((2.*nodes(j,1).*a.^2 + 16.*nodes(j,1).*nodes(j,2).^2 + 16.*nodes(j,1).*nodes(j,3).^2).^2./4 + (2.*nodes(j,2).*a.^2 + 16.*nodes(j,2).*nodes(j,1).^2 + 16.*nodes(j,2).*nodes(j,3).^2).^2./4 + (2.*nodes(j,3).*a.^2 + 16.*nodes(j,3).*nodes(j,1).^2 + 16.*nodes(j,3).*nodes(j,2).^2).^2./4).^(1./2)), (2.*nodes(j,2).*a.^2 + 16.*nodes(j,2).*nodes(j,1).^2 + 16.*nodes(j,2).*nodes(j,3).^2)./(2.*((2.*nodes(j,1).*a.^2 + 16.*nodes(j,1).*nodes(j,2).^2 + 16.*nodes(j,1).*nodes(j,3).^2).^2./4 + (2.*nodes(j,2).*a.^2 + 16.*nodes(j,2).*nodes(j,1).^2 + 16.*nodes(j,2).*nodes(j,3).^2).^2./4 + (2.*nodes(j,3).*a.^2 + 16.*nodes(j,3).*nodes(j,1).^2 + 16.*nodes(j,3).*nodes(j,2).^2).^2./4).^(1./2)), (2.*nodes(j,3).*a.^2 + 16.*nodes(j,3).*nodes(j,1).^2 + 16.*nodes(j,3).*nodes(j,2).^2)./(2.*((2.*nodes(j,1).*a.^2 + 16.*nodes(j,1).*nodes(j,2).^2 + 16.*nodes(j,1).*nodes(j,3).^2).^2./4 + (2.*nodes(j,2).*a.^2 + 16.*nodes(j,2).*nodes(j,1).^2 + 16.*nodes(j,2).*nodes(j,3).^2).^2./4 + (2.*nodes(j,3).*a.^2 + 16.*nodes(j,3).*nodes(j,1).^2 + 16.*nodes(j,3).*nodes(j,2).^2).^2./4).^(1./2))];
    end
    surfacename=strcat('GoursatOcta_','a=',num2str(a),'h=',num2str(h));
end

    

end

