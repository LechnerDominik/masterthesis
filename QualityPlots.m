% Skript das die verschiedenen Qualitätsplots erzeugt.




% %% Dumbbell
% tau=3*10^-3;
% Tend=0.078
% T=Tend/tau;
% for i=1:T+1
%     load(strcat("Result_MCF_Lubich_ALE/Dumbbell_h=0.1_solution_at_t",num2str((i-1)*tau),"_BDF2_tau0.003.mat"))
%     Qual_Dumbbell_Kovacs_ALE(:,i,1)=qual(:,1);
%     Qual_Dumbbell_Kovacs_ALE(:,i,2)=qual(:,2);
%     
%     load(strcat("Result_MCF_Lubich/Dumbbell_h=0.1_solution_at_t",num2str((i-1)*tau),"_BDF2_tau0.003.mat"))
%     Qual_Dumbbell_Kovacs(:,i,1)=qual(:,1);
%     Qual_Dumbbell_Kovacs(:,i,2)=qual(:,2);
%     
%     load(strcat("Result_MCF_Dziuk_ALE/Dumbbell_h=0.1_solution_at_t",num2str((i-1)*tau),"_BDF2_tau0.003.mat"))
%     Qual_Dumbbell_Dziuk_ALE(:,i,1)=qual(:,1);
%     Qual_Dumbbell_Dziuk_ALE(:,i,2)=qual(:,2);
%     
%     load(strcat("Result_MCF_Dziuk/Dumbbell_h=0.1_solution_at_t",num2str((i-1)*tau),"_BDF2_tau0.003.mat"))
%     Qual_Dumbbell_Dziuk(:,i,1)=qual(:,1);
%     Qual_Dumbbell_Dziuk(:,i,2)=qual(:,2);
%     
% end
% 
% figure;
% 
% plot(max(Qual_Dumbbell_Kovacs_ALE(:,:,1))./min(Qual_Dumbbell_Kovacs_ALE(:,:,1)),'--s')
% hold on
% plot(max(Qual_Dumbbell_Kovacs(:,:,1))./min(Qual_Dumbbell_Kovacs(:,:,1)),'--s')
% 
% plot(max(Qual_Dumbbell_Kovacs_ALE(:,:,1)),':o')
% plot(max(Qual_Dumbbell_Kovacs(:,:,1)),':o')
% 
% plot(min(Qual_Dumbbell_Kovacs_ALE(:,:,1)),'-.d')
% plot(min(Qual_Dumbbell_Kovacs(:,:,1)),'-.d')
% 
% legend('max/minAle','max/min','maxAle','max','minAle','min','location','best')
% title("Kov\'acs et al.: $$\frac{max(s_{i})}{r_{in}}$$",'interpreter','latex')
% xlabel('Zeitschritte')
% 
% print -depsc2 -painters Plots_Quality/Kovacs_Dumbbell_Time.eps
% 
% 
% figure;
% 
% plot(max(Qual_Dumbbell_Dziuk_ALE(:,:,1))./min(Qual_Dumbbell_Dziuk_ALE(:,:,1)),'--s')
% hold on
% plot(max(Qual_Dumbbell_Dziuk(:,:,1))./min(Qual_Dumbbell_Dziuk(:,:,1)),'--s')
% 
% plot(max(Qual_Dumbbell_Dziuk_ALE(:,:,1)),':o')
% plot(max(Qual_Dumbbell_Dziuk(:,:,1)),':o')
% 
% plot(min(Qual_Dumbbell_Dziuk_ALE(:,:,1)),'-.d')
% plot(min(Qual_Dumbbell_Dziuk(:,:,1)),'-.d')
% 
% legend('max/minAle','max/min','maxAle','max','minAle','min','location','best')
% title('Dziuk: $$\frac{max(s_{i})}{r_{in}}$$','interpreter','latex')
% xlabel('Zeitschritte')
% 
% print -depsc2 -painters Plots_Quality/Dziuk_Dumbbell_Time.eps
% 
% 
% figure
% prop=histogram(Qual_Dumbbell_Dziuk(:,1,2));
% hold on
% histogram(Qual_Dumbbell_Dziuk_ALE(:,end,2),'BinWidth',prop.BinWidth);
% histogram(Qual_Dumbbell_Dziuk(:,end,2),'BinWidth',prop.BinWidth);
% 
% legend('Anfangsgitter','ALE-Gitter','Gitter ohne ALE','Location','best')
% title('Dziuk: Gitter Qualit\"at: $$\frac{2r_{in}}{r_{out}}$$','interpreter','latex')
% ylabel('Anzahl Dreiecke')
% 
% print -dpng -painters Plots_Quality/Dziuk_Dumbbell_Histogramm.png
% 
% 
% figure
% prop=histogram(Qual_Dumbbell_Kovacs(:,1,2));
% hold on
% histogram(Qual_Dumbbell_Kovacs_ALE(:,end,2),'BinWidth',prop.BinWidth);
% histogram(Qual_Dumbbell_Kovacs(:,end,2),'BinWidth',prop.BinWidth);
% 
% legend('Anfangsgitter','ALE-Gitter','Gitter ohne ALE','Location','best')
% title(strcat("Kov\'acs et al. ", 'Gitter Qualit\"at: $$\frac{2r_{in}}{r_{out}}$$'),'interpreter','latex')
% ylabel('Anzahl Dreiecke')
% 
% print -dpng -painters Plots_Quality/Kovacs_Dumbbell_Histogramm.png
% 


% %% Goursat
% tau=2*10^-3;
% Tend=0.4
% T=Tend/tau;
% for i=1:T+1
%     load(strcat("Result_MCF_Lubich_ALE/GoursatOcta_a=2h=0.15_solution_at_t",num2str((i-1)*tau),"_BDF3_tau0.002.mat"))
%     Qual_Goursat_Kovacs_ALE(:,i,1)=qual(:,1);
%     Qual_Goursat_Kovacs_ALE(:,i,2)=qual(:,2);
%     
%     load(strcat("Result_MCF_Lubich/GoursatOcta_a=2h=0.15_solution_at_t",num2str((i-1)*tau),"_BDF3_tau0.002.mat"))
%     Qual_Goursat_Kovacs(:,i,1)=qual(:,1);
%     Qual_Goursat_Kovacs(:,i,2)=qual(:,2);
%     
%     load(strcat("Result_MCF_Dziuk_ALE/GoursatOcta_a=2h=0.15_solution_at_t",num2str((i-1)*tau),"_BDF3_tau0.002.mat"))
%     Qual_Goursat_Dziuk_ALE(:,i,1)=qual(:,1);
%     Qual_Goursat_Dziuk_ALE(:,i,2)=qual(:,2);
%     
%     load(strcat("Result_MCF_Dziuk/GoursatOcta_a=2h=0.15_solution_at_t",num2str((i-1)*tau),"_BDF3_tau0.002.mat"))
%     Qual_Goursat_Dziuk(:,i,1)=qual(:,1);
%     Qual_Goursat_Dziuk(:,i,2)=qual(:,2);
%     
% end
% 
% figure;
% 
% plot(max(Qual_Goursat_Kovacs_ALE(:,:,1))./min(Qual_Goursat_Kovacs_ALE(:,:,1)),'--s')
% hold on
% plot(max(Qual_Goursat_Kovacs(:,:,1))./min(Qual_Goursat_Kovacs(:,:,1)),'--s')
% 
% plot(max(Qual_Goursat_Kovacs_ALE(:,:,1)),':o')
% plot(max(Qual_Goursat_Kovacs(:,:,1)),':o')
% 
% plot(min(Qual_Goursat_Kovacs_ALE(:,:,1)),'-.d')
% plot(min(Qual_Goursat_Kovacs(:,:,1)),'-.d')
% 
% legend('max/minAle','max/min','maxAle','max','minAle','min','location','best')
% title("Kov\'acs et al.: $$\frac{max(s_{i})}{r_{in}}$$",'interpreter','latex')
% xlabel('Zeitschritte')
% 
% print -depsc2 -painters Plots_Quality/Kovacs_Goursat_Time.eps
% figure;
% 
% plot(max(Qual_Goursat_Dziuk_ALE(:,:,1))./min(Qual_Goursat_Dziuk_ALE(:,:,1)),'--s')
% hold on
% plot(max(Qual_Goursat_Dziuk(:,:,1))./min(Qual_Goursat_Dziuk(:,:,1)),'--s')
% 
% plot(max(Qual_Goursat_Dziuk_ALE(:,:,1)),':o')
% plot(max(Qual_Goursat_Dziuk(:,:,1)),':o')
% 
% plot(min(Qual_Goursat_Dziuk_ALE(:,:,1)),'-.d')
% plot(min(Qual_Goursat_Dziuk(:,:,1)),'-.d')
% 
% legend('max/minAle','max/min','maxAle','max','minAle','min','location','best')
% title('Dziuk: $$\frac{max(s_{i})}{r_{in}}$$','interpreter','latex')
% xlabel('Zeitschritte')
% 
% print -depsc2 -painters Plots_Quality/Dziuk_Goursat_Time.eps
% 
% 
% figure
% prop=histogram(Qual_Goursat_Dziuk(:,1,2));
% hold on
% histogram(Qual_Goursat_Dziuk_ALE(:,end,2),'BinWidth',prop.BinWidth);
% histogram(Qual_Goursat_Dziuk(:,end,2),'BinWidth',prop.BinWidth);
% 
% legend('Anfangsgitter','ALE-Gitter','Gitter ohne ALE','Location','best')
% title('Dziuk: Gitter Qualit\"at: $$\frac{2r_{in}}{r_{out}}$$','interpreter','latex')
% ylabel('Anzahl Dreiecke')
% 
% print -dpng -painters Plots_Quality/Dziuk_Goursat_Histogramm.png
% 
% 
% 
% figure
% prop=histogram(Qual_Goursat_Kovacs(:,1,2));
% hold on
% histogram(Qual_Goursat_Kovacs_ALE(:,end,2),'BinWidth',prop.BinWidth);
% histogram(Qual_Goursat_Kovacs(:,end,2),'BinWidth',prop.BinWidth);
% 
% legend('Anfangsgitter','ALE-Gitter','Gitter ohne ALE','Location','best')
% title(strcat("Kov\'acs et al. ", 'Gitter Qualit\"at: $$\frac{2r_{in}}{r_{out}}$$'),'interpreter','latex')
% ylabel('Anzahl Dreiecke')
% 
% print -dpng -painters Plots_Quality/Kovacs_Goursat_Histogramm.png
% 
% 
% %% Ellipsoid
% tau=4*10^-3;
% Tend=0.096
% T=Tend/tau;
% for i=1:T+1
%     load(strcat("Result_MCF_Lubich_ALE/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t",num2str((i-1)*tau),"_BDF2_tau0.004.mat"))
%     Qual_Ellipsoid_Kovacs_ALE(:,i,1)=qual(:,1);
%     Qual_Ellipsoid_Kovacs_ALE(:,i,2)=qual(:,2);
%     
%     load(strcat("Result_MCF_Lubich/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t",num2str((i-1)*tau),"_BDF2_tau0.004.mat"))
%     Qual_Ellipsoid_Kovacs(:,i,1)=qual(:,1);
%     Qual_Ellipsoid_Kovacs(:,i,2)=qual(:,2);
%     
%     load(strcat("Result_MCF_Dziuk_ALE/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t",num2str((i-1)*tau),"_BDF2_tau0.004.mat"))
%     Qual_Ellipsoid_Dziuk_ALE(:,i,1)=qual(:,1);
%     Qual_Ellipsoid_Dziuk_ALE(:,i,2)=qual(:,2);
%     
%     load(strcat("Result_MCF_Dziuk/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t",num2str((i-1)*tau),"_BDF2_tau0.004.mat"))
%     Qual_Ellipsoid_Dziuk(:,i,1)=qual(:,1);
%     Qual_Ellipsoid_Dziuk(:,i,2)=qual(:,2);
%     
% end
% 
% figure;
% 
% plot(max(Qual_Ellipsoid_Kovacs_ALE(:,:,1))./min(Qual_Ellipsoid_Kovacs_ALE(:,:,1)),'--s')
% hold on
% plot(max(Qual_Ellipsoid_Kovacs(:,:,1))./min(Qual_Ellipsoid_Kovacs(:,:,1)),'--s')
% 
% plot(max(Qual_Ellipsoid_Kovacs_ALE(:,:,1)),':o')
% plot(max(Qual_Ellipsoid_Kovacs(:,:,1)),':o')
% 
% plot(min(Qual_Ellipsoid_Kovacs_ALE(:,:,1)),'-.d')
% plot(min(Qual_Ellipsoid_Kovacs(:,:,1)),'-.d')
% 
% legend('max/minAle','max/min','maxAle','max','minAle','min','location','best')
% title("Kov\'acs et al.:$$\frac{max(s_{i})}{r_{in}}$$",'interpreter','latex')
% xlabel('Zeitschritte')
% 
% print -depsc2 -painters Plots_Quality/Kovacs_Ellipsoid_Time.eps
% 
% figure;
% 
% plot(max(Qual_Ellipsoid_Dziuk_ALE(:,:,1))./min(Qual_Ellipsoid_Dziuk_ALE(:,:,1)),'--s')
% hold on
% plot(max(Qual_Ellipsoid_Dziuk(:,:,1))./min(Qual_Ellipsoid_Dziuk(:,:,1)),'--s')
% 
% plot(max(Qual_Ellipsoid_Dziuk_ALE(:,:,1)),':o')
% plot(max(Qual_Ellipsoid_Dziuk(:,:,1)),':o')
% 
% plot(min(Qual_Ellipsoid_Dziuk_ALE(:,:,1)),'-.d')
% plot(min(Qual_Ellipsoid_Dziuk(:,:,1)),'-.d')
% 
% legend('max/minAle','max/min','maxAle','max','minAle','min','location','best')
% title('Dziuk:$$\frac{max(s_{i})}{r_{in}}$$','interpreter','latex')
% xlabel('Zeitschritte')
% 
% print -depsc2 -painters Plots_Quality/Dziuk_Ellipsoid_Time.eps
% 
% 
% figure
% prop=histogram(Qual_Ellipsoid_Dziuk(:,1,2));
% hold on
% histogram(Qual_Ellipsoid_Dziuk_ALE(:,end,2),'BinWidth',prop.BinWidth);
% histogram(Qual_Ellipsoid_Dziuk(:,end,2),'BinWidth',prop.BinWidth);
% 
% legend('Anfangsgitter','ALE-Gitter','Gitter ohne ALE','Location','best')
% title('Dziuk: Gitter Qualit\"at: $$\frac{2r_{in}}{r_{out}}$$','interpreter','latex')
% ylabel('Anzahl Dreiecke')
% 
% print -dpng -painters Plots_Quality/Dziuk_Ellipsoid_Histogramm.png
% 
% 
% figure
% prop=histogram(Qual_Ellipsoid_Kovacs(:,1,2));
% hold on
% histogram(Qual_Ellipsoid_Kovacs_ALE(:,end,2),'BinWidth',prop.BinWidth);
% histogram(Qual_Ellipsoid_Kovacs(:,end,2),'BinWidth',prop.BinWidth);
% 
% legend('Anfangsgitter','ALE-Gitter','Gitter ohne ALE','Location','best')
% title(strcat("Kov\'acs et al. ", 'Gitter Qualit\"at: $$\frac{2r_{in}}{r_{out}}$$'),'interpreter','latex')
% ylabel('Anzahl Dreiecke')
% 
% print -dpng -painters Plots_Quality/Kovacs_Ellipsoid_Histogramm.png

%% Quality Surfs

% Dumbbell
figure
subplot(1,2,1)
load("Result_MCF_Dziuk/Dumbbell_h=0.1_solution_at_t0.078_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3),qual(:,2))
axis equal
view(90,0)
 zlabel('Zeitpunkt 0.078');
 colorbar('AxisLocation','in');

c1=caxis;

% print -depsc2 -painters Plots_Quality/Dziuk_Dumbbell_Qual_Surf.eps


subplot(1,2,2)
load("Result_MCF_Dziuk_ALE/Dumbbell_h=0.1_solution_at_t0.078_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3),qual(:,2))
axis equal
view(90,0)
 zlabel('Zeitpunkt 0.078');
 caxis(c1);
  colorbar('AxisLocation','in');


print -depsc2 -painters Plots_Quality/Dziuk_ALE_Dumbbell_Qual_Surf.eps


figure
subplot(1,2,1)
load("Result_MCF_Lubich/Dumbbell_h=0.1_solution_at_t0.078_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3),qual(:,2))
axis equal
view(90,0)
 zlabel('Zeitpunkt 0.078');

 c1=caxis;
  colorbar('AxisLocation','in');



subplot(1,2,2)
load("Result_MCF_Lubich_ALE/Dumbbell_h=0.1_solution_at_t0.078_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3),qual(:,2))
axis equal
view(90,0)
 zlabel('Zeitpunkt 0.078');
caxis(c1);
  colorbar('AxisLocation','in');
 
print -depsc2 -painters Plots_Quality/Kovacs_ALE_Dumbbell_Qual_Surf.eps

% Goursat
figure
subplot(1,2,1)
load("Result_MCF_Dziuk/GoursatOcta_a=2h=0.15_solution_at_t0.4_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3),qual(:,2))
axis equal
view(-69,17)
zlabel('Zeitpunkt 0.4');
   colorbar('AxisLocation','in','Location','north');


c1=caxis;
% print -depsc2 -painters Plots_Quality/Dziuk_Goursat_Qual_Surf.eps


subplot(1,2,2)
load("Result_MCF_Dziuk_ALE/GoursatOcta_a=2h=0.15_solution_at_t0.4_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3),qual(:,2))
axis equal
view(-69,17)
zlabel('Zeitpunkt 0.4');
caxis(c1);
  colorbar('AxisLocation','in','Location','north');



print -dpng -painters Plots_Quality/Dziuk_ALE_Goursat_Qual_Surf.png

figure
subplot(1,2,1)
load("Result_MCF_Lubich/GoursatOcta_a=2h=0.15_solution_at_t0.4_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3),qual(:,2))
axis equal
view(-69,17)
zlabel('Zeitpunkt 0.4');
  colorbar('AxisLocation','in','Location','north');

c1=caxis;
% print -depsc2 -painters Plots_Quality/Kovacs_Goursat_Qual_Surf.eps



subplot(1,2,2)
load("Result_MCF_Lubich_ALE/GoursatOcta_a=2h=0.15_solution_at_t0.4_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3),qual(:,2))
axis equal
view(-69,17)
zlabel('Zeitpunkt 0.4');
caxis(c1);
  colorbar('AxisLocation','in','Location','north');

print -dpng -painters Plots_Quality/Kovacs_ALE_Goursat_Qual_Surf.png

%Ellipsoid

figure
subplot(1,2,1)
load("Result_MCF_Lubich/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.096_BDF2_tau0.004.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3),qual(:,2))
axis equal
view(-50,50)
zlabel('Zeitpunkt 0.096')
  colorbar('AxisLocation','in','Location','north');


c1=caxis;
print -dpng -painters Plots_Quality/Kovacs_Ellipsoid_Qual_Surf.png

subplot(1,2,2)
load("Result_MCF_Lubich_ALE/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.096_BDF2_tau0.004.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3),qual(:,2))
axis equal
view(-50,50)
zlabel('Zeitpunkt 0.096')
 caxis(c1)
  colorbar('AxisLocation','in','Location','north');



print -dpng -painters Plots_Quality/Kovacs_ALE_Ellipsoid_Qual_Surf.png

figure
subplot(1,2,1)
load("Result_MCF_Dziuk/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.096_BDF2_tau0.004.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3),qual(:,2))
axis equal
view(-50,50)
zlabel('Zeitpunkt 0,096')

  colorbar('AxisLocation','in','Location','north');

c1=caxis;
% print -depsc2 -painters Plots_Quality/Dziuk_Ellipsoid_Qual_Surf.eps



subplot(1,2,2)
load("Result_MCF_Dziuk_ALE/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.096_BDF2_tau0.004.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3),qual(:,2))
axis equal
view(-50,50)
zlabel('Zeitpunkt 0,096')
caxis(c1);
  colorbar('AxisLocation','in','Location','north');

print -dpng -painters Plots_Quality/Dziuk_ALE_Ellipsoid_Qual_Surf.png



figure
subplot(1,3,1)
load("Result_MCF_Lubich_ALE/Dumbbell_h=0.1_solution_at_t0.078_BDF2_tau0.001.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(90,0)
grid minor
 zlabel('Zeitpunkt 0.078');
 ylabel('\tau_1 =10^{-3}')
subplot(1,3,2)
load("Result_MCF_Lubich_ALE/Dumbbell_h=0.1_solution_at_t0.078_BDF2_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(90,0)
grid minor
 zlabel('Zeitpunkt 0.078');
 ylabel('\tau_2 =2*10^{-3}')
 
 subplot(1,3,3)
 load("Result_MCF_Lubich_ALE/Dumbbell_h=0.1_solution_at_t0.078_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(90,0)
grid minor
 zlabel('Zeitpunkt 0.078');
  ylabel('\tau_3 =3*10^{-3}')

  
print -dpng -painters Plots_Quality/Kovacs_ALE_Dumbbel_TimeComparison.png


figure
subplot(1,3,1)
load("Result_MCF_Lubich_ALE/Dumbbell_h=0.05_solution_at_t0.078_BDF2_tau0.001.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(90,0)
grid minor
 zlabel('Zeitpunkt 0.078');
 ylabel('\tau_1 =10^{-3}')
subplot(1,3,2)
load("Result_MCF_Lubich_ALE/Dumbbell_h=0.05_solution_at_t0.078_BDF2_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(90,0)
grid minor
 zlabel('Zeitpunkt 0.078');
 ylabel('\tau_2 =2*10^{-3}')
 subplot(1,3,3)
 load("Result_MCF_Lubich_ALE/Dumbbell_h=0.05_solution_at_t0.078_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(90,0)
grid minor
 zlabel('Zeitpunkt 0.078');
  ylabel('\tau_3 =3*10^{-3}')

  
print -dpng -painters Plots_Quality/Kovacs_FinerGrid_ALE_Dumbbel_TimeComparison.png





figure
subplot(1,2,1)
load('Result_MCF_Lubich_ALE/GoursatOcta_a=2h=0.15_solution_at_t0.4_BDF3_tau0.002.mat')
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-90,90)
zlabel('Zeitpunkt 0.4');
ylabel('Ansicht von oben mit 2286 Knoten, 4568 Elemente')
grid minor


subplot(1,2,2)
load('Result_MCF_Lubich_ALE/GoursatOcta_a=2h=0.075_solution_at_t0.4_BDF3_tau0.002.mat')
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-90,90)
zlabel('Zeitpunkt 0.4');
ylabel('Ansicht von oben mit 9054 Knoten, 18104 Elemente')
grid minor

print -dpng -painters Plots_Quality/Kovacs_ALE_Goursat_Space_Comparison.png



figure
subplot(1,2,1)
load('Result_MCF_Dziuk_ALE/GoursatOcta_a=2h=0.15_solution_at_t0.4_BDF3_tau0.002.mat')
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-90,0)
zlabel('Zeitpunkt 0.4');
ylabel('Frontansicht mit 2286 Knoten, 4568 Elemente')
grid minor


subplot(1,2,2)
load('Result_MCF_Dziuk_ALE/GoursatOcta_a=2h=0.075_solution_at_t0.4_BDF3_tau0.002.mat')
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-90,0)
zlabel('Zeitpunkt 0.4');
ylabel('Frontansicht mit 9054 Knoten, 18104 Elemente')
grid minor

print -dpng -painters Plots_Quality/Dziuk_ALE_Goursat_Space_Comparison.png

close all