% 
% 
% clear all
% 
% load("Result_MCF_Dziuk/Dumbbell_h=0.1_solution_at_t0_BDF2_tau0.003.mat")
% trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
%  title('Dziuk')
% zlabel('Zeitpunkt 0');
% axis equal
% 
% set(gca,'nextplot','replacechildren'); 
% 
% % Create a video writer object for the output video file and open the object for writing.
% 
% v = VideoWriter('Dumbbell_Dziuk.avi');
% v.Quality=100;
% 
% open(v);
% 
% set(gcf, 'Position', get(0, 'Screensize'));
% 
% % Generate a set of frames, get the frame from the figure, and then write each frame to the file.
% tau=3*10^-3;
% for k = 1:27
% load(strcat("Result_MCF_Dziuk/Dumbbell_h=0.1_solution_at_t",num2str((k-1)*tau),"_BDF2_tau0.003.mat"))
% subplot(1,2,1)
% trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
%  title('Dziuk')
% zlabel(strcat('Zeitpunkt',num2str((k-1)*tau)));
% axis equal
% subplot(1,2,2)
% load(strcat("Result_MCF_Dziuk_ALE/Dumbbell_h=0.1_solution_at_t",num2str((k-1)*tau),"_BDF2_tau0.003.mat"))
% trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
%  title('Dziuk ALE')
% zlabel(strcat('Zeitpunkt',num2str((k-1)*tau)));
% axis equal
%     frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
% end
% 
% close(v);




%%%%%%%%%%


% clear all
% 
% load("Result_MCF_Dziuk/Dumbbell_h=0.1_solution_at_t0_BDF2_tau0.003.mat")
% trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
%  title('Dziuk')
% zlabel('Zeitpunkt 0');
% axis equal
% 
% set(gca,'nextplot','replacechildren'); 
% 
% % Create a video writer object for the output video file and open the object for writing.
% 
% v = VideoWriter('Dumbbell_Kovacs_FinerRes.avi');
% v.Quality=100;
% 
% open(v);
% 
% set(gcf, 'Position', get(0, 'Screensize'));
% 
% % Generate a set of frames, get the frame from the figure, and then write each frame to the file.
% tau=3*10^-3;
% for k = 1:27
% load(strcat("Result_MCF_Lubich/Dumbbell_h=0.05_solution_at_t",num2str((k-1)*tau),"_BDF2_tau0.003.mat"))
% subplot(1,2,1)
% trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
%  title('Kov�cs et al.')
% zlabel(strcat('Zeitpunkt',num2str((k-1)*tau)));
% axis equal
% subplot(1,2,2)
% load(strcat("Result_MCF_Lubich_ALE/Dumbbell_h=0.05_solution_at_t",num2str((k-1)*tau),"_BDF2_tau0.003.mat"))
% trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
%  title('Kov�cs et al. ALE')
% zlabel(strcat('Zeitpunkt',num2str((k-1)*tau)));
% axis equal
%     frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
%         frame = getframe(gcf);
%     writeVideo(v,frame);
% end
% 
% close(v);

%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all

load("Result_MCF_Lubich/GoursatOcta_a=2h=0.1_solution_at_t0_BDF3_tau0.002.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
 title('Dziuk')
zlabel('Zeitpunkt 0');
axis equal

set(gca,'nextplot','replacechildren'); 

% Create a video writer object for the output video file and open the object for writing.

v = VideoWriter('Goursat_Kovacs.avi');
v.Quality=100;

open(v);

set(gcf, 'Position', get(0, 'Screensize'));

% Generate a set of frames, get the frame from the figure, and then write each frame to the file.
tau=2*10^-3;
for k = 1:201
load(strcat("Result_MCF_Lubich/GoursatOcta_a=2h=0.075_solution_at_t",num2str((k-1)*tau),"_BDF3_tau0.002.mat"))
subplot(1,2,1)
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
 title('Kov�cs et al.')
zlabel(strcat('Zeitpunkt',num2str((k-1)*tau)));
axis equal
subplot(1,2,2)
load(strcat("Result_MCF_Lubich_ALE/GoursatOcta_a=2h=0.075_solution_at_t",num2str((k-1)*tau),"_BDF3_tau0.002.mat"))
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
 title('Kov�cs et al. ALE')
zlabel(strcat('Zeitpunkt',num2str((k-1)*tau)));
axis equal
    frame = getframe(gcf);
    writeVideo(v,frame);
        frame = getframe(gcf);
    writeVideo(v,frame);
        frame = getframe(gcf);
    writeVideo(v,frame);
        frame = getframe(gcf);
    writeVideo(v,frame);
        frame = getframe(gcf);
    writeVideo(v,frame);
        frame = getframe(gcf);
    writeVideo(v,frame);

end

close(v);






