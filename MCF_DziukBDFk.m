function [qual,x,elements]=MCF_DziukBDFk(k,tau,Tend,nodes,elements,ALE,surfacename)

% Dziuk's algorithm (1990) for mean Curvature flow (MCF).

format long;

%% Inputs & Parameters
% k=3;            % BDF method
% tau=0.01;       % time stepsize
% T
% 

%% initial surface

% degrees of freedom
dof=length(nodes);

% saving initial mesh
nodes0=nodes;
elements0=elements;

ALECounter=0;

%% initial values
% initial values are obtained by hand
j=0;
counter=0;
t=[0];
x_neu=nodes0;
x(:,:,1)=x_neu;
[qual(:,1),qual(:,2)] = Qmesh(x_neu,elements,2);

 %% saving
    s1.t_neu=t;
    s1.x_neu=x_neu;
    s1.elements=elements;
    s1.qual=qual;
    s1.ALE=ALE;
    if strcmp(ALE.active,'on')
    save(['Result_MCF_Dziuk_ALE/',surfacename,'_solution_at_t',num2str(t),'_BDF',num2str(k),'_tau',num2str(tau),'.mat'],'-struct','s1');
    else
    save(['Result_MCF_Dziuk/',surfacename,'_solution_at_t',num2str(t),'_BDF',num2str(k),'_tau',num2str(tau),'.mat'],'-struct','s1');
    end




for i=1:k
    [delta,gamma]=BDF_tableau(i);
    % initial data for k=1 obtained by a linearly implicit backward Euler step
    t_neu=i*tau;
    
        % extrapolations
    t_tilde=0;
    x_tilde=zeros(dof,3);
    for j=1:i
        % time as validation
        t_add=t(end-i+j);
        t_tilde=t_tilde+gamma(i-j+1)*t_add;
        % surface
        x_tilde=x_tilde+gamma(i-j+1)*x(:,:,end-i+j);
    end
    
    
    
    % assembly of the evolving stiffness and mass matrix on the EXTRAPOLATED surface
    [A,M]=surface_assembly(x_tilde,elements);
    
    % contributions from the past
    % rho_v
        dx=0*x_tilde;
    for j=1:i
        dx=dx+delta(j+1)*x(:,:,end-j+1);
    end
    
    Mdx(:,1)=M*dx(:,1);
    Mdx(:,2)=M*dx(:,2);
    Mdx(:,3)=M*dx(:,3);
    rho_v=-Mdx;

    % solving linear systems
    x_neu=(delta(1)*M+tau*A)\rho_v;
    x(:,:,i+1)=x_neu;
    % new time
    t=[t t_neu];
    [qual(:,1),qual(:,2)] = Qmesh(x_neu,elements,2);
    % plot
 %% saving
    s1.t_neu=t_neu;
    s1.x_neu=x_neu;
    s1.elements=elements;
    s1.qual=qual;
    s1.ALE=ALE;
    if strcmp(ALE.active,'on')
    save(['Result_MCF_Dziuk_ALE/',surfacename,'_solution_at_t',num2str(t_neu),'_BDF',num2str(k),'_tau',num2str(tau),'.mat'],'-struct','s1');
    else
    save(['Result_MCF_Dziuk/',surfacename,'_solution_at_t',num2str(t_neu),'_BDF',num2str(k),'_tau',num2str(tau),'.mat'],'-struct','s1');
    end
    
    
    
end
    

%% time Integration
for steps=k+1:ceil(Tend/tau)
counter=counter+1;
    % timestep
t_neu=t(end)+tau;

    % extrapolations
    t_tilde=0;
    x_tilde=zeros(dof,3);
    for j=1:k
        % time as validation
        t_add=t(end-k+j);
        t_tilde=t_tilde+gamma(k-j+1)*t_add;
        % surface
        x_tilde=x_tilde+gamma(k-j+1)*x(:,:,end-k+j);
    end

    % assembly of the evolving stiffness and mass matrix on the EXTRAPOLATED surface
    [A,M]=surface_assembly(x_tilde,elements);
    
    % contributions from the past
    % rho_v
    dx=0*x_tilde;
    for j=1:k
        dx=dx+delta(j+1)*x(:,:,end-j+1);
    end
    Mdx(:,1)=M*dx(:,1);
    Mdx(:,2)=M*dx(:,2);
    Mdx(:,3)=M*dx(:,3);
    rho_v=-Mdx;
    
    % solving the decoupled linear systems
    x_neu=(delta(1)*M+tau*A)\rho_v;
    %% ALE
if strcmp(ALE.active,'on') &(ALECounter<ceil((round(Tend/tau)-k)*ALE.part))

[x_neu,Alesteps,elements]=ALEmap_stepModified2dDziuk(x_neu,elements,x_neu,ALE.h,ALE.integrator,ALE.deltat,ALE.strategy );

ALECounter=ALECounter+1;
% Some Plotting
% trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3),H)
%  title(strcat('Ale-mesh at t=', num2str(t*tau)))
% 
% colorbar;
% hold on
% % quiver3(x_neu(:,1),x_neu(:,2),x_neu(:,3),u_neu(:,1),u_neu(:,2),u_neu(:,3));
% hold off
% axis equal
% drawnow
end
    

    % updating the solutions and the time vector
    % new surface
    x(:,:,steps+1)=x_neu;
    
    [qual(:,1),qual(:,2)] = Qmesh(x(:,:,steps+1),elements,2);
%     qual(steps,:)=[max(cc),min(cc)];
    % new time
    t=[t t_neu];
    %% saving
    s1.t_neu=t_neu;
    s1.x_neu=x_neu;
    s1.elements=elements;
    s1.qual=qual;
    s1.ALE=ALE;
    if strcmp(ALE.active,'on')
    save(['Result_MCF_Dziuk_ALE/',surfacename,'_solution_at_t',num2str(t_neu),'_BDF',num2str(k),'_tau',num2str(tau),'.mat'],'-struct','s1');
    else
    save(['Result_MCF_Dziuk/',surfacename,'_solution_at_t',num2str(t_neu),'_BDF',num2str(k),'_tau',num2str(tau),'.mat'],'-struct','s1');
    end
    
    
    
 
end
