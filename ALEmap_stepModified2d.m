function [Points,steps,t,u_neu]=ALEmap_stepModified2d(nodes,elements,Points,h0,integrator,deltat,strategy,u,varargin)

% Modified by Dominik Lechner, doesnt need a signed distance function
% anymore. Signed distance function was replaced by a linear projection
% onto the elements. To speed up this process, you can change algorithm =
% 'linear' to algorithm='parallel'
%
% Written by Bal�zs Kov�cs. Computing arbitrary Lagrangian Eulerian maps for evolving surfaces. NMPDE. doi:10.1002/num.22340
% Based on DistMesh by Per-Olof Persson and Gilbert Strang
%   Copyright of DistMesh (C) 2004-2012 Per-Olof Persson.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%ALEswitch='on'; % Until it degrades and will be sitched off

dptol=1e-2; ttol=.1;% deps=sqrt(eps)*h0;
steps=0;
counter=0
% Fscale=1.1;  deltat=.05;
% ...

% Elements for checking for possible retriangulation
t=elements;

% Connectivities (for trisurfupd)

 [t2t,t2n]=mkt2t(t);
 t2t=int32(t2t-1)'; t2n=int8(t2n-1)';

N=size(Points,1);                                         % Number of points N
Points_old=inf;                                            % For first iteration

% figure
    
% steps=0;
 while 1 
    steps=steps+1;
    p0=Points;
    % 3. Retriangulation
    if max(sqrt(sum((Points-Points_old).^2,2))/h0)>ttol           % Any large movement?
        Points_old=Points;                                          % Save current positions
        
        counter=counter+1
 

         [t,t2t,t2n]=trisurfupd(int32(t-1)',t2t,t2n,Points');  % Update triangles
           t=double(t+1)'; % +1 and -1 are because C++ subrutine...
   
%         pmid=(Points(t(:,1),:)+Points(t(:,2),:)+Points(t(:,3),:))/3;    % Compute centroids

        % 4. Describe each bar by a unique pair of nodes
        Bars=[t(:,[1,2]);t(:,[1,3]);t(:,[2,3])];         % Interior bars duplicated
        Bars=unique(sort(Bars,2),'rows');                % Bars as node pairs

%         %     % 5. Graphical output of the current mesh
%             clf,patch('faces',t,'vertices',Points,'facecol',[.8,.9,1],'edgecol','k');
%             axis equal;axis off;view(3);cameratoolbar;drawnow

    end

    % 6. Move mesh points based on bar lengths L and forces F

    Points=time_integrator_step2d(deltat,Points,Bars,integrator,strategy,t);

    % 7. Bring all points back to the boundary

Algorithm='parallel';

[Points,~,Nu_neu,H_neu] = LinProject(nodes,elements,Points,Algorithm,u);
u_neu=[Nu_neu./vecnorm(Nu_neu,2,2),H_neu'];

    % 8. Termination criterion: All nodes move less than dptol (scaled)
    if max(sqrt(sum((Points-p0).^2,2))/h0) < dptol
 
        return;
     
    end
 end
