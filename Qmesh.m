function [qualA,qualB] = Qmesh(nodes,elements,mode)
% Input: nodes,elements and a mode to compute some quality measures for
% triangles
% Mode 1:
%Computs radius r and max length of heigth h of triangle to compute c=r/h
% Mode else:
% Computes 2*innerRadius/outerRadius
% Author: Dominik Lechner


qualA=zeros(length(elements),1);
qualB=zeros(length(elements),1);
if mode==1
    for j=1:length(elements)
    a=sqrt((nodes(elements(j,1),1)-nodes(elements(j,2),1))^2+(nodes(elements(j,1),2)-nodes(elements(j,2),2))^2+(nodes(elements(j,1),3)-nodes(elements(j,2),3))^2);
    b=sqrt((nodes(elements(j,1),1)-nodes(elements(j,3),1))^2+(nodes(elements(j,1),2)-nodes(elements(j,3),2))^2+(nodes(elements(j,1),3)-nodes(elements(j,3),3))^2);
    c=sqrt((nodes(elements(j,2),1)-nodes(elements(j,3),1))^2+(nodes(elements(j,2),2)-nodes(elements(j,3),2))^2+(nodes(elements(j,2),3)-nodes(elements(j,3),3))^2);
    qualB(j)=((b+c-a)*(c+a-b)*(a+b-c))/(a*b*c);
    end
else
for j=1:length(elements)
    %side lengths a,b,c
a=sqrt((nodes(elements(j,1),1)-nodes(elements(j,2),1))^2+(nodes(elements(j,1),2)-nodes(elements(j,2),2))^2+(nodes(elements(j,1),3)-nodes(elements(j,2),3))^2);
b=sqrt((nodes(elements(j,1),1)-nodes(elements(j,3),1))^2+(nodes(elements(j,1),2)-nodes(elements(j,3),2))^2+(nodes(elements(j,1),3)-nodes(elements(j,3),3))^2);
c=sqrt((nodes(elements(j,2),1)-nodes(elements(j,3),1))^2+(nodes(elements(j,2),2)-nodes(elements(j,3),2))^2+(nodes(elements(j,2),3)-nodes(elements(j,3),3))^2);
s=(a+b+c)/2;
A=sqrt(s*(s-a)*(s-b)*(s-c));  %Area
R=(a*b*c)/(4*A); % Outer circle radius
r=sqrt(((s-a)*(s-b)*(s-c))/s); %inner circle radius
h=max([(b*c)/(2*R),(a*c)/(2*R),(b*a)/(2*R)]); %max height
qualA(j)=h/r;
qualB(j)=((b+c-a)*(c+a-b)*(a+b-c))/(a*b*c);
end
end
end
