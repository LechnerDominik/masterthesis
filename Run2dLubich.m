clear all
%% PDE settings
tstart=0;
tend=0.4;
Tspan=[tstart,tend];
k=3;            % BDF method
tau=2*10^-3; %

%% Mesh settings ellipsoid
mode=4; %1=ellipsoid 2=dumbbell 3=GoursatCube 4=GoursatOctahedron
h=0.075; %Mindestens 0.3
param.a=2;param.b=1/2;param.c=1.5;

%% ALE settingsALE.active='on';
ALE.integrator='RK4'; %EE , iEE, RK4
ALE.deltat=0.1;
ALE.strategy='lp'; %disc, cont ,cut , lp , distmesh
ALE.h=h/10;
ALE.part=1; % number between [0,1] gives roundabout the first part where ale steps are performed
%% Calculations
%Creates an Ellipsoid
[nodes,elements,Nu,H,surfacename] = surfacemesh(mode,h,param);



%% Lubich
tic
ALE.active='off';
[x,u,qual]=MCF_Lubich2D(k,tau,tend,nodes,elements,H,Nu,ALE,surfacename);
toc
% 
% 
% 
% tic
% ALE.active='on';
% [xAle,u,qualAle,Elements]=MCF_Lubich2D(k,tau,tend,nodes,elements,H,Nu,ALE,surfacename);
% toc

%% Dziuk
% % 
%  ALE.active='off';
%  [qual,x,elements]=MCF_DziukBDFk(k,tau,tend,nodes,elements,ALE,surfacename);
% 
% 
% ALE.active='on';
% [qual,x,elements]=MCF_DziukBDFk(k,tau,tend,nodes,elements,ALE,surfacename);

%Plotting

figure;

plot(max(qualAle(:,:,1))./min(qualAle(:,:,1)),'--s')
hold on
plot(max(qual(:,:,1))./min(qual(:,:,1)),'--s')

plot(max(qualAle(:,:,1)),':o')
plot(max(qual(:,:,1)),':o')

plot(min(qualAle(:,:,1)),'-.d')
plot(min(qual(:,:,1)),'-.d')




legend('max/minAle','max/min','maxAle','max','minAle','min')
title('$$\frac{max(s_{i})}{r_{in}}$$','interpreter','latex')
xlabel('Zeitschritte')

figure
prop=histogram(qual(:,1,2));
hold on
histogram(qualAle(:,end,2),'BinWidth',prop.BinWidth);
histogram(qual(:,end,2),'BinWidth',prop.BinWidth);

legend('Anfang','Ale Ergebnis','Ergebnis ohne Ale')
title('Gitter Qualit\"at: $$\frac{2r_{in}}{r_{out}}$$','interpreter','latex')
ylabel('Anzahl Dreiecke')



%% Plotting to debug
% figure
% for t=1:round(tend/tau)+1
% subplot(1,3,1) 
% trisurf(elements,x(:,1,t),x(:,2,t),x(:,3,t),qual(:,t,2));
% title(strcat('Mesh at t=', num2str((t-1)*tau)));
% axis equal
% colorbar;
% 
% 
% c1=caxis;
% 
% 
% subplot(1,3,2) %bottom view
% trisurf(elements,x(:,1,t),x(:,2,t),x(:,3,t),qual(:,t,2));
% view([0,90]);
% title(strcat('View form Top: Mesh at t=', num2str((t-1)*tau)));
% axis equal;
% subplot(1,3,3)
% prop=histogram(qual(:,1,2));
% hold on;
% histogram(qual(:,end,2),'BinWidth',prop.BinWidth);
% legend('Anfang','Ergebnis ohne Ale')
% title('Gitter Qualit\"at: $$\frac{2r_{in}}{r_{out}}$$','interpreter','latex')
% ylabel('Anzahl Dreiecke')
% waitforbuttonpress
% end
% 
% figure
% for t=1:round(tend/tau)+1
% 
% caxis(c1);
% 
% subplot(1,3,1);
% trisurf(Elements,xAle(:,1,t),xAle(:,2,t),xAle(:,3,t),qualAle(:,t,2));
% title(strcat('Ale-mesh at t=', num2str((t-1)*tau)));
% axis equal;
% colorbar;
% caxis(c1);
% 
% subplot(1,3,2); %bottomview
% trisurf(Elements,xAle(:,1,t),xAle(:,2,t),xAle(:,3,t),qualAle(:,t,2));
% view([0,90]);
% title(strcat('View from Top: Ale-mesh at t=', num2str((t-1)*tau)));
% axis equal
% 
% caxis(c1);
% subplot(1,3,3)
% prop=histogram(qual(:,1,2));
% hold on;
% histogram(qualAle(:,end,2),'BinWidth',prop.BinWidth);
% legend('Anfang','Ergebnis Ale')
% title('Gitter Qualit\"at: $$\frac{2r_{in}}{r_{out}}$$','interpreter','latex')
% ylabel('Anzahl Dreiecke')
% waitforbuttonpress
% end

%% Plot for picture
figure
[~,~,t]=size(x);
subplot(1,3,1) 
trisurf(elements,x(:,1,t),x(:,2,t),x(:,3,t),qual(:,t,2));
title(strcat('Mesh at t=', num2str((t-1)*tau)));
axis equal
colorbar;


c1=caxis;


subplot(1,3,2) %bottom view
trisurf(elements,x(:,1,t),x(:,2,t),x(:,3,t),qual(:,t,2));
view([0,90]);
title(strcat('View form Top: Mesh at t=', num2str((t-1)*tau)));
axis equal;
subplot(1,3,3)
prop=histogram(qual(:,1,2));
hold on;
histogram(qual(:,end,2),'BinWidth',prop.BinWidth);
legend('Anfang','Ergebnis ohne Ale')
title('Gitter Qualit\"at: $$\frac{2r_{in}}{r_{out}}$$','interpreter','latex')
ylabel('Anzahl Dreiecke')


figure

[~,~,t]=size(xAle);
caxis(c1);

subplot(1,3,1);
trisurf(Elements,xAle(:,1,t),xAle(:,2,t),xAle(:,3,t),qualAle(:,t,2));
title(strcat('Ale-mesh at t=', num2str((t-1)*tau)));
axis equal;
colorbar;
caxis(c1);

subplot(1,3,2); %bottomview
trisurf(Elements,xAle(:,1,t),xAle(:,2,t),xAle(:,3,t),qualAle(:,t,2));
view([0,90]);
title(strcat('View from Top: Ale-mesh at t=', num2str((t-1)*tau)));
axis equal

caxis(c1);
subplot(1,3,3)
prop=histogram(qual(:,1,2));
hold on;
histogram(qualAle(:,end,2),'BinWidth',prop.BinWidth);
legend('Anfang','Ergebnis Ale')
title('Gitter Qualit\"at: $$\frac{2r_{in}}{r_{out}}$$','interpreter','latex')
ylabel('Anzahl Dreiecke')

