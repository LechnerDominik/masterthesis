function [f,g1,g2] = loadvector2d(nodes,elements,u)
% Function takes nodes elements mean curvature and normal vector of a
% surface and calculates the loadvectors f, g
% Code was based on Bal�zs Kov�cs implementation for quadratic finite
% elements but modified by Dominik Lechner for standard finite elements.


format long;
% the quadrature nodes and the number of quadrature nodes
W = [0.072157803838894   0.047545817133642   0.047545817133642   0.047545817133642   0.051608685267359   0.051608685267359   0.051608685267359 0.016229248811599   0.016229248811599   0.016229248811599   0.013615157087217   0.013615157087217   0.013615157087217   0.013615157087217 0.013615157087217   0.013615157087217];
length_W = length(W);

%% load or compute and save the (precomputed) product tensors
if exist(['loadvect_precomp/loadvect_precomp',num2str(length_W),'.mat'], 'file')==2
    precomp_values=load(['loadvect_precomp/loadvect_precomp',num2str(length_W),'.mat']);
    F_eval = precomp_values.F_eval;
    grad_F_eval = precomp_values.grad_F_eval;
else
    disp(['Computing and storing precomputeable values (M=',num2str(length_W),').'])
    % Dunavant quadrature of order 5 nodes and weights
    xieta = [0.333333333333333   0.081414823414554   0.459292588292723   0.459292588292723   0.658861384496480   0.170569307751760   0.170569307751760   0.898905543365938   0.050547228317031   0.050547228317031   0.008394777409958   0.263112829634638   0.728492392955404   0.263112829634638   0.728492392955404   0.008394777409958;
        0.333333333333333   0.459292588292723   0.459292588292723   0.081414823414554   0.170569307751760   0.170569307751760   0.658861384496480   0.050547228317031   0.050547228317031   0.898905543365938   0.263112829634638   0.728492392955404   0.008394777409958   0.008394777409958   0.263112829634638   0.728492392955404;
        0                   0                   0                   0                   0                   0                   0                   0                   0                   0                   0                   0                   0                   0                   0                   0;];
    % precomputing data
    [F_eval, grad_F_eval] = loadvector2d_preprocess(xieta,W);
    % saving data in the right directory, and creating directory if it not exists
    if exist('loadvect_precomp', 'dir') ~= 7
        mkdir('loadvect_precomp');
    end
    save(['loadvect_precomp/loadvect_precomp',num2str(length_W),'.mat'],'F_eval','grad_F_eval');
end

% Initialisierung
n=length(nodes);


f=zeros(n,4);
g1=zeros(n,3);
g2=zeros(n,3);

array_columns_3 = reshape([1 2 3] .* ones(1,3).',[9 1]);
array_columns_4 = reshape([1 2 3 4] .* ones(1,3).',[12 1]);

%iterate over all elements
for i=1:length(elements)
    
    % element row arrays for vectors (dimension: n x 3 and n x 4)
    array_rows_3 = reshape(ones(1,3) .* elements(i,:).',[9 1]);
    array_rows_4 = reshape(ones(1,4) .* elements(i,:).',[12 1]);
    
    ind3=sub2ind(size(g1),array_rows_3,array_columns_3);
    ind4=sub2ind(size(f),array_rows_4,array_columns_4);
    
    % nodes of i-th element
    nodes_loc=nodes(elements(i,:),:);
    
    % u-values for i-th element
    u_loc=u(elements(i,:),:);
    
    % Linear Map, its inverse C
    % L is slightly permutated, because of F_eval
    ortho=cross((nodes_loc(2,:)-nodes_loc(1,:))',(nodes_loc(3,:)-nodes_loc(1,:))');
    orthonorm=norm(ortho);
    
    L = [(nodes_loc(1,:)-nodes_loc(3,:))',(nodes_loc(2,:)-nodes_loc(3,:))',ortho];
    C = inv(L);
    
    
    alpha_h=norm(C.' * (grad_F_eval(:,:,1) * u_loc(:,1:3)),'fro');
    
    prod_W_nrm=(W.*orthonorm).';
    
    
    % Eval basis Functions
    B=C.'*grad_F_eval(:,:,1);
    BtB=B.'*B;
    
    prod_W_nrm_alpha_u_loc=(prod_W_nrm .* (alpha_h.^2)) .* (F_eval * u_loc);
    
    % f
    fLoc_vect = reshape((prod_W_nrm_alpha_u_loc.' * F_eval).',[12 1]);
    
    
    
    f(ind4)=f(ind4)+fLoc_vect;
    
    
    % g1
    prod_W_nrm_alpha_u_loc = prod_W_nrm .* ( F_eval * u_loc(:,1:3) ) .* ( F_eval * u_loc(:,4) );
    
    g1Loc_vect = reshape((prod_W_nrm_alpha_u_loc.' * F_eval).',[9 1]);
    
    g1(ind3)=g1(ind3)+g1Loc_vect;
    
    
    %% g2
    prod_W_deter_H_loc = prod_W_nrm .* ( F_eval * u_loc(:,4) );
    
    for i=1:length(W)
        prod_BtB_nu_loc(:,:,i) = BtB * u_loc(:,1:3);
    end
    
    prod_W_deter_nu_loc = prod_W_nrm .* ( F_eval * u_loc(:,1:3) );
    
    for i=1:length(W)
        prod_BtB_H_loc(:,:,i) = BtB * u_loc(:,4);
    end
    g2Loc_vect = reshape(prod_BtB_nu_loc,[9 length_W]) * prod_W_deter_H_loc ...
        + reshape ( reshape(prod_BtB_H_loc,[3 length_W]) * prod_W_deter_nu_loc , [9 1]);
    
    
    g2(ind3)=g2(ind3)+g2Loc_vect;
    
     
    
end

end

%% 
function [F_eval, grad_F_eval]=loadvector2d_preprocess(xieta,W)
% Precomputes Basisevalutions and its gradient on qadrature nodes
%
% - standard reference element (with nodes (0,0), (1,0), (0,1),
% - standard finite elements with basis xi, eta, 1-xi-eta

% basis functions
F=@(xi,eta)[xi,eta,1-xi-eta];

% evaluating the basis functions on the reference element
F_eval=[];
for i=1:length(W)
    F_eval=[F_eval ; F(xieta(1,i),xieta(2,i))];
end

for i=1:length(W)
    F_ell=F_eval(i,:);
end

% Evaluating gradients of the basis functions in the quadrature nodes,
% gradients were manually computed
% grad F1
grad_F1=@(xi,eta) [1;0;0];
% grad F2
grad_F2=@(xi,eta) [0;1;0];
% grad F3
grad_F3=@(xi,eta) [-1;-1;0];

for i=1:length(W)
    grad_F_eval(:,:,i)=[grad_F1(xieta(1,i),xieta(2,i)) grad_F2(xieta(1,i),xieta(2,i)) grad_F3(xieta(1,i),xieta(2,i))];
end
end