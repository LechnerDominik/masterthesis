

function [Points,distance,u_neu,H_neu] = LinProject(nodes,elements,Points,Algorithm,u)

%input nodes, elements of a surface, 
%Points that have been moved by some forces and need to be projected back
%to the surface
% u some nodal function living on the nodes
%Algorithm 'linear' 
 %         'parallel'

 %Output: distance to surface
 %         SurfacePoints: projeceted Points
 %         quantity interpolated to the position of the projected points
 %         used during calculations
 %          u_neu,H_neu linearly interpolated version of u
 %         
 %          Author:Dominik Lechner
 
 SubSurface=1;
 
  
% Calculate normals 
n1 = nodes(elements(:,1),:);   % 1st nodes of every element
n2 = nodes(elements(:,2),:);   % 2nd nodes of every element
n3 = nodes(elements(:,3),:);   % 3rd nodes of every element
normals = cross((n2-n1),(n3-n1),2); % normal vector of every element
normals = normals./sqrt(sum(normals.^2,2)); %Normalisation

%% Distance Calculation
nPoints = size(Points,1);


Nu=u(:,1:3);
H=u(:,4);

D = NaN(nPoints,1);
P = NaN(nPoints,3);
switch Algorithm
    case{'linear'}
        for r = 1:nPoints
            % Determine the surface points
            point = Points(r,:); % query point
            [d,p,Nu_new,H_new] = queryPoint(elements,nodes,point,normals, SubSurface,Nu,H);
            D(r) = d;
            P(r,:) = p;
            u_neu(r,:)=Nu_new;
            H_neu(r)=H_new;
        end
    case{'parallel'}
        parfor r = 1:nPoints
            point = Points(r,:); % query point
            [d,p,Nu_new,H_new] = queryPoint(elements,nodes,point,normals, SubSurface,Nu,H);
            D(r) = d;
            P(r,:) = p;
            u_neu(r,:)=Nu_new;
            H_neu(r)=H_new;
        end
end

% return output arguments
distance      = D;  % (#qPoints x 1)
Points = P;  % (#qPoints x 3)
end

function [d,p,Nu_new,H_new]=queryPoint(elements,nodes,point,normals,SubSurface,Nu,H)

[d(1),p(1,:),Nu(1,:),H(1)]=point2point(nodes,point,Nu,H);

if SubSurface==1
    [triangles]=subsurface(p(1,:),nodes,elements); %vector: zahl gibt element zeile an.
end


edges = [elements(triangles,[1,2]); elements(triangles,[1,3]); elements(triangles,[2,3])]; % edges:  2xnodes IDs
edges = unique(edges,'rows');  %Kick out double counts
edgePoints{:,1} = nodes(edges(:,1),:);   
edgePoints{:,2} = nodes(edges(:,2),:);
edgeNu{:,1}=Nu(edges(:,1),:); edgeNu{:,2}=Nu(edges(:,2),:);
edgeH{:,1}=H(edges(:,1)); edgeH{:,2}=H(edges(:,2));

[d(2),p(2,:),Nu(2,:),H(2)]=point2edge(edgePoints, point,edgeNu,edgeH);

[d(3),p(3,:),Nu(3,:),H(3)]=point2triangle(nodes,elements,triangles,normals,point,Nu,H);

[d,I]=min(d);
p=p(I,:);
Nu_new=Nu(I,:);
H_new=H(I);
% trimesh(elements(triangles,:),nodes(:,1),nodes(:,2),nodes(:,3))
% hold on 
% plot3(point(1),point(2),point(3),'ro')
% drawnow
% plot3(p(1),p(2),p(3),'k*')
% hold off

end

function [triangles]=subsurface(p,nodes,elements)
triangles=[];
subnodes=[p];

[lengthP,~]=size(p);
for j=1:lengthP
ind=find(sum(p(j,:)==nodes,2)==3); %find row where p== nodes
[row,~]=ind2sub(size(elements),find(elements==ind));
[triangles,ia,ic]=unique([triangles;row],'rows'); % Add outerlayer
%subelements=unique([subelements;elements(row,:)],'rows');
%subnodes=unique([subnodes;nodes(setdiff(unique(subelements),ind),:)],'rows');
%triangles=triangles(ia);
end
p=subnodes;

%for debugplots
% subelements=unique(subelements,'rows');
% subnodes=unique(subnodes,'rows');
%trimesh(subelements,nodes(subelements(:,1),:),nodes(subelements(:,2),:),nodes(subelements(:,3),:));
% trimesh(elements(triangles,:),nodes(:,1),nodes(:,2),nodes(:,3))
% hold on 
% plot3(p(1),p(2),p(3),'r*')
% drawnow
% hold off
end




function [d,p,Nu_new,H_new]=point2point(nodes,point,Nu,H)
dist=((nodes(:,1)-point(:,1)).^2+(nodes(:,2)-point(:,2)).^2+(nodes(:,3)-point(:,3)).^2);
[d,I]=min(dist);
d=sqrt(d);
p=nodes(I,:);
Nu_new=Nu(I,:);
H_new=H(I);
end



function [d,p,Nu_new,H_new]=point2edge(edgePoints, point,edgeNu,edgeH)
%edges ist eine 1x2 celle, mit zeilenweise verschiedenen Kanten,
%spaltenweise die unterschiedlichen Punkte

v=edgePoints{:,2}-edgePoints{:,1};
a=point-edgePoints{:,1};
t=dot(a,v,2)./dot(v,v,2);

t(t<0)=NaN; t(t>1)=NaN;
proj=edgePoints{:,1}+t.*v;
projNu=edgeNu{:,1}+t.*(edgeNu{:,2}-edgeNu{:,1});
projH= edgeH{:,1}+t.*(edgeH{:,2}-edgeH{:,1});
dist=(proj(:,1)-point(:,1)).^2+(proj(:,2)-point(:,2)).^2+(proj(:,3)-point(:,3)).^2;
[d,I]=min(dist);
d=sqrt(d);
p=proj(I,:);
Nu_new=projNu(I,:);
H_new=projH(I);
end



function [d,p,Nu_new,H_new]=point2triangle(nodes,elements,triangles,normals,point,Nu,H)
% triangle is a list of numbers containing rows of elements
ptemp=zeros(length(triangles),3);
Nutemp=zeros(length(triangles),3);
Htemp=zeros(length(triangles),1);
%trimesh(elements(triangles,:),nodes(:,1),nodes(:,2),nodes(:,3));
%hold on
for i=1:length(triangles)
   n1=nodes(elements(triangles(i),1),:)';
   n2=nodes(elements(triangles(i),2),:)';
   n3=nodes(elements(triangles(i),3),:)';
   
   Nu1=Nu(elements(triangles(i),1),:)';
   Nu2=Nu(elements(triangles(i),2),:)';
   Nu3=Nu(elements(triangles(i),3),:)';
   
   H1=H(elements(triangles(i),1),:)';
   H2=H(elements(triangles(i),2),:)';
   H3=H(elements(triangles(i),3),:)';
   
   
    N=normals(triangles(i),:)';
    distloc=N'*(n1-point');
    
    proj=point'+distloc*N; %projected point
    dist(i)=(proj(1)-point(1))^2+(proj(2)-point(2))^2+(proj(3)-point(3))^2; %distance 
    v0=(n3-n1); %for more readability
    v1=(n2-n1);
    v2=(proj-n1);
    
         
            
            
    %Check with barycentric coordiantes if point is inside triangle
    lambda=[v0'*v0, v1'*v0; 
            v0'*v1,v1'*v1]\[v2'*v0;v2'*v1];
        if lambda(1)>=0 & lambda(2)>=0 & sum(lambda)<=1
            ptemp(i,:)=proj';
            Nutemp(i,:)=Nu1'+ lambda(2)*(Nu2'-Nu1')+ lambda(1)*(Nu3'-Nu1');
            Htemp(i)=H1+lambda(2)*(H2-H1)+lambda(1)*(H3-H1);
    
               %plotting for debug
%                      plot3(proj(1),proj(2),proj(3),'*');
%             quiver3(n1(1),n1(2),n1(3),Nu1(1),Nu1(2),Nu1(3),0.25);
%             quiver3(n2(1),n2(2),n2(3),Nu2(1),Nu2(2),Nu2(3),0.25);
%             quiver3(n3(1),n3(2),n3(3),Nu3(1),Nu3(2),Nu3(3),0.25);
%             quiver3(proj(1),proj(2),proj(3),Nutemp(i,1),Nutemp(i,2),Nutemp(i,3),0.30);
            
            
            
        else
            ptemp(i,:)=NaN;
            dist(i)=NaN;
            Nutemp(i,:)=NaN;
            Htemp(i,:)=NaN;
                       
        end
end
%hold off
[d,I]=min(dist);
d=sqrt(d);
p=ptemp(I,:);
Nu_new=Nutemp(I,:);
H_new=Htemp(I,:);
end






