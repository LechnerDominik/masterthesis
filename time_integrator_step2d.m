function Points_out=time_integrator_step2d(deltat,Points,Bars,integrator,strategy,elements)
% Modified by Dominik Lechner.
% Based on Bal�zs Kov�cs. Computing arbitrary Lagrangian Eulerian maps for evolving surfaces. NMPDE. doi:10.1002/num.22340
% which was based on Based on DistMesh by Per-Olof Persson and Gilbert Strang
%   Copyright of DistMesh (C) 2004-2012 Per-Olof Persson.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% calculate r.h.s.
%% Update node positions
if strcmp(integrator,'EE')
    % forward Euler step
    Ftot = func_Ftot(Points,Bars,strategy,elements);
    Points_out=Points+deltat*Ftot;
elseif strcmp(integrator,'iEE')
    % improved forward Euler step
    Ftot_1 = func_Ftot(Points,Bars,strategy,elements);
    Ftot_2 = func_Ftot(Points+0.5*deltat*Ftot_1,Bars,strategy,elements);
    Points_out=Points+deltat*Ftot_2;
elseif strcmp(integrator,'RK4')
    % the classical Runge-Kutta 4 method
    Ftot_1=func_Ftot(Points,Bars,strategy,elements);
    Ftot_2=func_Ftot(Points+0.5*deltat*Ftot_1,Bars,strategy,elements);
    Ftot_3=func_Ftot(Points+0.5*deltat*Ftot_2,Bars,strategy,elements);
    Ftot_4=func_Ftot(Points+1.0*deltat*Ftot_3,Bars,strategy,elements);
    Points_out=Points+deltat/6*(Ftot_1+2*Ftot_2+2*Ftot_3+Ftot_4);
end

function Ftot = func_Ftot(Points,Bars,strategy,elements)

N=size(Points,1);

barvec=Points(Bars(:,1),:)-Points(Bars(:,2),:);    % List of Bar vectors
L=sqrt(sum(barvec.^2,2));                          % L = Bar lengths

% L0 = Desired lengths
L0 = L;
if strcmp(strategy,'disc')
    % discontinuous multiplier
    L0(L>=0.8*max(L))=0.8*L(L>=0.8*max(L));
    L0(L<1.2*min(L))=1.2*L(L<1.2*min(L));
    % Bar forces (scalars)
F=L0-L;
elseif strcmp(strategy,'cut')
    % cut multiplier
    L0(L>=min(L)+0.6*(max(L)-min(L)))=min(L)+0.6*(max(L)-min(L));
    L0(L< min(L)+0.4*(max(L)-min(L)))=min(L)+0.4*(max(L)-min(L));
    % Bar forces (scalars)
F=L0-L;
elseif strcmp(strategy,'cont')
    l=linspace(-1,1,length(L0))';
    l=0.2*(-l).^5+1;
    L0=L.*l;
    % Bar forces (scalars)
F=L0-L;
elseif strcmp(strategy,'lp')
    % from paper but basically the same as the cut multiplier
    p=0.5;
    L0(L>min(L)+(1-p)*(max(L)-min(L)))=min(L)+(1-p)*(max(L)-min(L));
    L0(L<=min(L)+(1-p)*(max(L)-min(L)))=min(L)+p*(max(L)-min(L));
    % Bar forces (scalars)
F=L0-L;
elseif strcmp(strategy,'distmesh')
      hbars=ones(length(Bars),1);
      Fscale=1.2; %As in distmesh
  L0=hbars*Fscale*sqrt(sum(L.^2)/sum(hbars.^2));     % L0 = Desired lengths
 % Bar forces (scalars)
  F=max(L0-L,0);
end



Fvec=F./L*[1,1,1].*barvec;                         % Bar forces (x,y,z components)
Ftot=full(sparse(Bars(:,[1,1,1,2,2,2]),ones(size(F))*[1,2,3,1,2,3],[Fvec,-Fvec],N,3));
% 
% Debug PLots
% figure(2)
% 
% subplot(1,3,1)
% trisurf(elements,Points(:,1),Points(:,2),Points(:,3))
% view([90,75])
% drawnow
% title('surface')
% 
% subplot(1,3,2)
% quiver3(Points(:,1),Points(:,2),Points(:,3),Ftot(:,1),Ftot(:,2),Ftot(:,3));
% %view([90,75])
% drawnow
% %pause(0.5)
% title('Ftot')
% subplot(1,3,3)
% plot(F)
% drawnow
% title('Kraftmenge')

% sparse sums up for repeated indeces
disp('')