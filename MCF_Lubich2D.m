function [x,u,qual,elements]=MCF_Lubich2D(k,tau,Tend,nodes,elements,H,Nu,ALE,surfacename)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lubich, Kovacs, Li's algorithm (2019) for Mean Curvature flow (MCF).
%
% Solves this algorithm for a surface given by nodes, elements,Mean curvature H, normal Nu
% with a BDF -k method form Tstart=0 up to Tend
% ALE method is optional
% surfacename is just a string to save the results

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Initialisierung
n=length(nodes);
x_neu=nodes;
u_neu=[Nu(:,1),Nu(:,2),Nu(:,3),H];
x(:,:,1)=x_neu;
u(:,:,1)=u_neu;
ALECounter=0;

tt=0; 
[qual(:,1),qual(:,2)] = Qmesh(x_neu,elements,2);


  %% Saving 
  
    s1.t_neu=tt;
    s1.x_neu=x_neu;
    s1.u_neu=u_neu;
    s1.elements=elements;
    s1.qual=qual;
    s1.ALE=ALE;
    
    if strcmp(ALE.active,'on')
    save(['Result_MCF_Lubich_ALE/',surfacename,'_solution_at_t',num2str(tt),'_BDF',num2str(k),'_tau',num2str(tau),'.mat'],'-struct','s1');
    else
    save(['Result_MCF_Lubich/',surfacename,'_solution_at_t',num2str(tt),'_BDF',num2str(k),'_tau',num2str(tau),'.mat'],'-struct','s1');
    end



%Initalvalues
steps=0;
for i=1:k-1
    
    t_neu = i*tau;
    tt=[tt t_neu];
    
    
    %%% Extrapolation
   [delta,gamma]=BDF_tableau(i) ;
   xtilde=zeros(n,3);
   utilde=zeros(n,4);
    for j=1:i
        xtilde=xtilde+gamma(i-j+1)*x(:,:,end-i+j);
        utilde=utilde+gamma(i-j+1)*u(:,:,end-i+j);
              
    end
[A,M]=surface_assembly(xtilde,elements); %Assembly matrices
[f,outg1,outg2] = loadvector2d(xtilde,elements,utilde); %Assembly loadvector
g=-outg1-outg2;



K=A+M;


dx=zeros(n,3);
du=zeros(n,4);
%BDF steps
for j=1:i
   dx=dx+delta(j+1)*x(:,:,end-j+1);
   du=du+delta(j+1)*u(:,:,end-j+1);
end

% Calculations
    rho_v=tau*g-K*dx;
    x_neu=(delta(1)*K)\rho_v;

    rho_u=tau*f-M*du;

    u_neu=(delta(1)*M+tau*A)\rho_u;


x(:,:,i+1)=x_neu;
u(:,:,i+1)=u_neu;
%Quality
 [qual(:,1),qual(:,2)] = Qmesh(x_neu,elements,2);

   %% Saving 
  
    s1.t_neu=t_neu;
    s1.x_neu=x_neu;
    s1.u_neu=u_neu;
    s1.elements=elements;
    s1.qual=qual;
    s1.ALE=ALE;
    if strcmp(ALE.active,'on')
    save(['Result_MCF_Lubich_ALE/',surfacename,'_solution_at_t',num2str(t_neu),'_BDF',num2str(k),'_tau',num2str(tau),'.mat'],'-struct','s1');
    else
    save(['Result_MCF_Lubich/',surfacename,'_solution_at_t',num2str(t_neu),'_BDF',num2str(k),'_tau',num2str(tau),'.mat'],'-struct','s1');
    end
 

end


%% timesteps
for t=k:round(Tend/tau)
    t_neu = tt(end)+tau;
    tt=[tt t_neu];
    
       %%% Extrapolation
   [delta,gamma]=BDF_tableau(k) ;
   xtilde=zeros(n,3);
   utilde=zeros(n,4);
    for j=1:k
        xtilde=xtilde+gamma(k-j+1)*x(:,:,end-k+j);
        utilde=utilde+gamma(k-j+1)*u(:,:,end-k+j);
        
          % normalisation
        utilde(:,1:3) = utilde(:,1:3) ./ (sum(utilde(:,1:3).^2,2)).^(0.5);
        
    end
[A,M]=surface_assembly(xtilde,elements);

[f,outg1,outg2] = loadvector2d(xtilde,elements,utilde);

g=-outg1-outg2;


K=A+M;


dx=zeros(n,3);
du=zeros(n,4);
for j=1:k
   dx=dx+delta(j+1)*x(:,:,end-j+1);
   du=du+delta(j+1)*u(:,:,end-j+1);
end


   rho_v=tau*g-K*dx;
   x_neu=(delta(1)*K)\rho_v;
    
   rho_u=tau*f-M*du;
   
   u_neu=(delta(1)*M+tau*A)\rho_u;
   u_neu(:,1:3) = u_neu(:,1:3) ./ (sum(u_neu(:,1:3).^2,2)).^(0.5);
    

    %%%% ALE Methode
        %% ALE
        
if strcmp(ALE.active,'on') &(ALECounter<ceil((round(Tend/tau)-k)*ALE.part))
[x_neu,Alesteps,elements,u_neu]=ALEmap_stepModified2d(x_neu,elements,x_neu,ALE.h,ALE.integrator,ALE.deltat,ALE.strategy,u_neu );

ALECounter=ALECounter+1;
% Some Plotting
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3),H)
 title(strcat('Ale-mesh at t=', num2str(t*tau)))

colorbar;
hold on
quiver3(x_neu(:,1),x_neu(:,2),x_neu(:,3),u_neu(:,1),u_neu(:,2),u_neu(:,3));
hold off
axis equal
drawnow
end
 steps=steps+1;   
    
    
 

 [qual(:,1),qual(:,2)] = Qmesh(x_neu,elements,2);

x(:,:,t+1)=x_neu;
u(:,:,t+1)=u_neu;
       %% Saving 
  
    s1.t_neu=t_neu;
    s1.x_neu=x_neu;
    s1.u_neu=u_neu;
    s1.elements=elements;
    s1.qual=qual;
    s1.ALE=ALE;
    if strcmp(ALE.active,'on')
    save(['Result_MCF_Lubich_ALE/',surfacename,'_solution_at_t',num2str(t_neu),'_BDF',num2str(k),'_tau',num2str(tau),'.mat'],'-struct','s1');
    else
    save(['Result_MCF_Lubich/',surfacename,'_solution_at_t',num2str(t_neu),'_BDF',num2str(k),'_tau',num2str(tau),'.mat'],'-struct','s1');
    end
          
    
end



end