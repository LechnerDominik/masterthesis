Matlab Code to my Masterthesis.
Evolving Surface Finite Element Method Implementation of Mean Curvature Flow. Algorithms used were by Kovács, Li, Lubich and by Dziuk.
Implementation comes with an ALE-version to maintain grid regularity. 
Results were saved as .mat files

Visualization can be watched here:
https://www.youtube.com/watch?v=zPziOjNm4JE
https://www.youtube.com/watch?v=c9Mp9yysjLQ
https://www.youtube.com/watch?v=Dp-elOWTnSQ
