syms x y z a b c 
% Author: Dominik Lechner
% Skript computes symbollically some geometric quantities for surfaces given by implicit equations
% Outputs used in surfacemesh.m

  %F=x^2/a^2+y^2/b^2+z^2/c^2-1 %Ellipsoid 
%F=x^2 + y^2+ 2*z^2*(z^2-199/200)-0.04  %Dumbbell
 %F=x^4+y^4+z^4-a^2*(x^2+y^2+z^2);  %Goursat Cube 
F=8*(x^2*y^2+y^2*z^2+z^2*x^2)+a^2*(x^2+y^2+z^2)-a^4;%Goursat Octahedron
GradF=[diff(F,'x'),diff(F,'y'),diff(F,'z')]
HessF=[diff(GradF,'x');diff(GradF,'y');diff(GradF,'z');]
H=(GradF*HessF*GradF.'-sum(GradF.^2)*trace(HessF))/(2*sqrt(sum((GradF).^2))^3)
Nu=GradF/sqrt(sum(GradF.^2))

J=[diff(Nu(1),'x'),diff(Nu(1),'y'),diff(Nu(1),'z');
    diff(Nu(2),'x'),diff(Nu(2),'y'),diff(Nu(2),'z');
    diff(Nu(3),'x'),diff(Nu(3),'y'),diff(Nu(3),'z');];
alpha_h=norm((J.'-Nu.'*(Nu*J.')),'fro')^2


 