function [A,M]=surface_assembly(nodes,elements)
% Calculates mass matrix M and stiffness matrix A for nodes and elements
% 
%   Author: Dominik Lechner: dominik.lechner94@gmail.com

% Basisnodes on reference triangle are (0,0), (1,0),(0,1)


[n1,~] = size(elements);
[n2,~] = size(nodes);
A = zeros(n2,n2);            % stiffness matrix
M = zeros(n2,n2);            % massmatrix


% local Matrices computed on reference triangle
M0 = [1/12,1/24,1/24;
    1/24,1/12,1/24;
    1/24,1/24,1/12];
Axx = 0.5*[1,-1,0;
    -1,1,0;
    0,0,0];
Axy = 0.5*[1,0,-1;
    -1,0,1;
    0,0,0];
Ayx = 0.5*[1,-1,0;
    0,0,0;
    -1,1,0];
Ayy = 0.5*[1,0,-1;
    0,0,0;
    -1,0,1];

for j=1:n1
    x1=nodes(elements(j,1),:)';
    x2=nodes(elements(j,2),:)';
    x3=nodes(elements(j,3),:)';
    % Transformation map
    ortho=cross(x2-x1,x3-x1);
    orthonorm=norm(ortho);
    L = [x2-x1,x3-x1,ortho./orthonorm];
    C = inv(L);
    
    %!use norm of normal Vector instead of determinant
    
 %   Determinante(j) = abs(det(L));   % determinant

    
    Mloc = orthonorm*M0;
    Aloc = orthonorm*((C(1,1)^2+C(1,2)^2+C(1,3)^2)*Axx + (C(2,1)^2+C(2,2)^2+C(2,3)^2)*Ayy + (C(1,1)*C(2,1)+C(1,2)*C(2,2)+C(1,3)*C(2,3))*(Axy+Ayx));
%                     
    
    % global matrices A and M
    A(elements(j,1:3),elements(j,1:3)) = A(elements(j,1:3),elements(j,1:3)) + Aloc;
    M(elements(j,1:3),elements(j,1:3)) = M(elements(j,1:3),elements(j,1:3)) + Mloc;
    
end
A=sparse(A);
M=sparse(M);
end
