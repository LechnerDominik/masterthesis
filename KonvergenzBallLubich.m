
addpath('distmesh')
clear all
profile on

trimesh3=@(elements,XYZ,varargin) trimesh(elements,XYZ(:,1),XYZ(:,2),XYZ(:,3),varargin{:});
% Script that computes MCF for a sphere of Radius 2 up to time 0.6 for
% different space & time discretisations and then calculates the error and
% plots it

%% Initializations
k=3; % Bdf k
hs=[0.5,0.3535,0.25,0.1767,0.125,0.08838,0.0625];%,0.0441];%,0.031250,0.022097,0.015625];
hs_computed = zeros(length(hs),1);
taus = [0.2,0.1,0.05,0.025,0.0125,0.00625,0.003125];
T_end = 0.6;
r0=2; %Startradius
errors_L2 = zeros(length(hs),length(taus));
errors_H1 = zeros(length(hs),length(taus));
errors_AM = zeros(length(hs),length(taus));

errorsH_AM = zeros(length(hs),length(taus));
errorsNu_AM = zeros(length(hs),length(taus));


%% Iterate over lengths
for h_count = 1:length(hs)
    h = hs(h_count);
    
            %% ALE settings but ALE is not used
ALE.active='on';
ALE.integrator='RK4'; %EE , iEE, RK4
ALE.deltat=0.1;
ALE.strategy='lp'; %disc, cont ,cut , lp , distmesh
ALE.h=h/10;
        
    
r=sqrt(r0^2-4*T_end); %Radius
%fdend=@(p) dsphere(p,0,0,0,r); %distance function
fdstart=@(p) dsphere(p,0,0,0,r0); %distance function
%[nodesSol,elementsSol]=distmeshsurface(fdend,@huniform,h,1.2*[-r,-r,-r;r,r,r]);
tic
[nodes,elements]=distmeshsurface(fdstart,@huniform,h,1.2*[-r0,-r0,-r0;r0,r0,r0]);
  Nu=nodes./vecnorm(nodes,2,2);
  H=ones(length(nodes),1)*2/r0;
  time = toc;
    fprintf('Mesh generation for h = %f completed after %f seconds.\n',hs(h_count),time);
    fprintf('The mesh has %d nodes and %d elements.\n',size(nodes,1),size(elements,1));
    
    
    for i=1:length(elements)
        triangle = nodes(elements(i,:),:);
        for ii=1:3
            if ii<3
                h_triangle = norm(triangle(1,:) - triangle(ii+1,:));
            else
                h_triangle = norm(triangle(2,:) - triangle(ii,:));
            end
            if h_triangle > hs_computed(h_count)
                hs_computed(h_count) = h_triangle;
            end
        end
    end
    surfacename=strcat('sphere_',num2str(hs_computed(h_count)));
    
    
    
 %% Iterate over all tau
 tic
    for j = 1:length(taus) % iterate over all tau
       tau=taus(j); 
        

%% Calculations

ALE.active='off';

[x,u]=MCF_Lubich2D(k,tau,T_end,nodes,elements,H,Nu,ALE,surfacename);

[A,M]=surface_assembly(x(:,:,end),elements);
            
lambda=zeros(length(x),1);
lambda=r^2./(x(:,1,end).^2+x(:,2,end).^2+x(:,3,end).^2);
xproj=lambda.*x(:,:,end);
Nu_korrekt=xproj./vecnorm(xproj,2,2);

        
        error=r*ones(length(x),1)-sqrt((x(:,1,end).^2+x(:,2,end).^2+x(:,3,end).^2));
        errors_L2(h_count,j) = sqrt(error'*M*error);
        errors_H1(h_count,j) = sqrt(error'*A*error);
        errors_AM(h_count,j)=sqrt(error'*(M+A)*error);

        errorH=u(:,4,end)-2/r*ones(length(x),1);
        errorsH_AM(h_count,j)=sqrt(errorH'*(M+A)*errorH);
        
        errorNu=vecnorm(u(:,1:3,end)-Nu_korrekt,2,2);
        errorsNu_AM(h_count,j)=sqrt(errorNu'*(M+A)*errorNu);
        
    end
time =toc;
fprintf('Solution for h = %f completed after %f seconds.\n',hs(h_count),time);
end

if k~=3
    warning('Plot beschriftung anpassen')
end
figure
sgtitle('Sph�re mit Radius 2 bis Tmax=0.6 BDF-3')
        ax1 = subplot(1,3,2);
        for a=1:length(hs_computed) 
            loglog(ax1,taus,errorsNu_AM(a,:))
            hold on
           
        end
         xlabel(ax1,'Schrittweite in der Zeit')
         ylabel(ax1,'Fehler')
        title(ax1,'$$\vert\vert{\nu-\nu_h}\vert\vert_{L^{\infty}(H^1)}$$','interpreter','latex')            
        loglog(taus,taus.^3,'--')
     legend('h = 0.5','h = 0.3535','h = 0.25','h = 0.1767','h = 0.125',...
            'h = 0.08838','h = 0.0625','O(\tau^3)','Location','best') %'h = 0.08838','h = 0.0625',
     
        
        ax2 = subplot(1,3,3);
        for a=1:length(hs_computed)
            loglog(ax2,taus,errorsH_AM(a,:))
            hold on
            xlabel(ax2,'Schrittweite in der Zeit')
            ylabel(ax2,'Fehler')
               
        end
                title(ax2,'$$\vert\vert{H-H_h}\vert\vert_{L^{\infty}(H^1)}$$','interpreter','latex')            

          loglog(taus,taus.^3,'--')
        legend('h = 0.5','h = 0.3535','h = 0.25','h = 0.1767','h = 0.125',...
            'h = 0.08838','h = 0.0625','O(\tau^3)','Location','best') %'h = 0.08838','h = 0.0625',
        
                ax3 = subplot(1,3,1);
        for a=1:length(hs_computed) 
            loglog(ax3,taus,errors_AM(a,:))
            hold on
         xlabel(ax3,'Schrittweite in der Zeit')
            ylabel(ax3,'Fehler')
            
        end
                title(ax3,'$$\vert\vert{X-X_h}\vert\vert_{L^{\infty}(H^1)}$$','interpreter','latex')            

        loglog(taus,taus.^2,'--')
         legend('h = 0.5','h = 0.3535','h = 0.25','h = 0.1767','h = 0.125',...
            'h = 0.08838','h = 0.0625','O(\tau^3)','Location','best') %'h = 0.08838','h = 0.0625',

        
        
        figure
     sgtitle('Sph�re mit Radius 2 bis Tmax=0.6 BDF-3')
        ax1 = subplot(1,3,2);
        for a=3:length(taus) 
            loglog(ax1,hs_computed,errorsNu_AM(:,a))
            hold on
            xlabel(ax1,'Schrittweite im Raum')
            ylabel(ax1,'Fehler')
           

        end
                title(ax1,'$$\vert\vert{\nu-\nu_h}\vert\vert_{L^{\infty}(H^1)}$$','interpreter','latex')            

          loglog(hs_computed,hs_computed.^2,'--')
           %legend('\tau = 0.2','\tau = 0.1',
           legend('\tau = 0.05','\tau = 0.025',...
            '\tau = 0.0125','\tau = 0.00625','\tau = 0.003125','O(h^2)','Location','best') 
        
    
             

        
                    ax2 = subplot(1,3,3);
        for a=3:length(taus)
            loglog(ax2,hs_computed,errorsH_AM(:,a))
            hold on
            xlabel(ax2,'Schrittweite im Raum')
            ylabel(ax2,'Fehler')
        
        end
                title(ax2,'$$\vert\vert{H-H_h}\vert\vert_{L^{\infty}(H^1)}$$','interpreter','latex')            

        loglog(hs_computed,hs_computed.^2,'--')
              %legend('\tau = 0.2','\tau = 0.1',
           legend('\tau = 0.05','\tau = 0.025',...
            '\tau = 0.0125','\tau = 0.00625','\tau = 0.003125','O(h^2)','Location','best') 
     
     
        ax3 = subplot(1,3,1);
        for a=3:length(taus) 
            loglog(ax3,hs_computed,errors_AM(:,a))
            hold on
            xlabel(ax3,'Schrittweite im Raum')
            ylabel(ax3,'Fehler')
           

        end
                title(ax3,'$$\vert\vert{X-X_h}\vert\vert_{L^{\infty}(H^1)}$$','interpreter','latex')            

          loglog(hs_computed,hs_computed.^2,'--')
               %legend('\tau = 0.2','\tau = 0.1',
           legend('\tau = 0.05','\tau = 0.025',...
            '\tau = 0.0125','\tau = 0.00625','\tau = 0.003125','O(h^2)','Location','best') 
     