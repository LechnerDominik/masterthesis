% t = tiledlayout(3,3);
% t.TileSpacing = 'compact';
% 
% t.Padding = 'compact';
% colormap parula
%% Dziuk plots
% t1=nexttile(1);

load("Result_MCF_Dziuk/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0_BDF2_tau0.004.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
title('Dziuk')
zlabel('Zeitpunkt 0');
axis equal
view(-50,50)
grid minor

print -depsc2 -painters Plots_Ellipsoid/Dziuk_Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0_BDF2_tau0.004.mat.eps


% t2=nexttile(4);
load("Result_MCF_Dziuk/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.096_BDF2_tau0.004.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-50,50)
zlabel('Zeitpunkt 0.096');
grid minor

print -depsc2 -painters Plots_Ellipsoid/Dziuk_Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.096_BDF2_tau0.004.mat.eps

% t3=nexttile(7);
%subplot(4,3,7)
load("Result_MCF_Dziuk/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.172_BDF2_tau0.004.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-50,50)
zlabel('Zeitpunkt 0.172');
grid minor

print -depsc2 -painters Plots_Ellipsoid/Dziuk_Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.172_BDF2_tau0.004.mat.eps

%% Dziuk ALE plots
% t1=nexttile(1);

load("Result_MCF_Dziuk_ALE/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0_BDF2_tau0.004.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
title('Dziuk ALE')
% zlabel('Zeitpunkt 0');
axis equal
view(-50,50)
grid minor

print -depsc2 -painters Plots_Ellipsoid/Dziuk_ALE_Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0_BDF2_tau0.004.mat.eps

% t2=nexttile(4);
load("Result_MCF_Dziuk_ALE/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.096_BDF2_tau0.004.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-50,50)
% zlabel('Zeitpunkt 0.096');
grid minor

print -depsc2 -painters Plots_Ellipsoid/Dziuk_ALE_Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.096_BDF2_tau0.004.mat.eps

% t3=nexttile(7);
%subplot(4,3,7)
load("Result_MCF_Dziuk_ALE/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.172_BDF2_tau0.004.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-50,50)
% zlabel('Zeitpunkt 0.172');
grid minor

print -depsc2 -painters Plots_Ellipsoid/Dziuk_ALE_Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.172_BDF2_tau0.004.mat.eps

%% Lubich plot
% nexttile(2)

% subplot(4,3,2)
load("Result_MCF_Lubich/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0_BDF2_tau0.004.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
title('Kov�cs et al.')
axis equal
view(-50,50)
grid minor

print -depsc2 -painters Plots_Ellipsoid/Kovacs_Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0_BDF2_tau0.004.mat.eps

% nexttile(5)
% subplot(4,3,5)
load("Result_MCF_Lubich/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.096_BDF2_tau0.004.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-50,50)
grid minor

print -depsc2 -painters Plots_Ellipsoid/Kovacs_Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.096_BDF2_tau0.004.mat.eps


% nexttile(8)
% subplot(4,3,8)
load("Result_MCF_Lubich/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.172_BDF2_tau0.004.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-50,50)
grid minor

print -depsc2 -painters Plots_Ellipsoid/Kovacs_Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.172_BDF2_tau0.004.mat.eps




%% Lubich ALE
% nexttile(3)
% subplot(4,3,3)
% subplot(4,3,2)
load("Result_MCF_Lubich_ALE/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0_BDF2_tau0.004.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
title('Kov�cs et al. ALE')
axis equal
view(-50,50)
grid minor

print -depsc2 -painters Plots_Ellipsoid/Kovacs_ALE_Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0_BDF2_tau0.004.mat.eps


% nexttile(5)
% subplot(4,3,5)
load("Result_MCF_Lubich_ALE/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.096_BDF2_tau0.004.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-50,50)
grid minor
print -depsc2 -painters Plots_Ellipsoid/Kovacs_ALE_Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.096_BDF2_tau0.004.mat.eps


% nexttile(8)
% subplot(4,3,8)
load("Result_MCF_Lubich_ALE/Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.172_BDF2_tau0.004.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(-50,50)
grid minor
print -depsc2 -painters Plots_Ellipsoid/Kovacs_ALE_Ellipsoid_a=1b=0.5c=1.5h=0.09_solution_at_t0.172_BDF2_tau0.004.mat.eps

