
% 
% t = tiledlayout(4,3);
% t.TileSpacing = 'compact';
% 
% t.Padding = 'compact';
figure

%% Dziuk plots
%t1=subplot(4,3,1)
load("Result_MCF_Dziuk/Dumbbell_h=0.1_solution_at_t0_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
 title('Dziuk')
zlabel('Zeitpunkt 0');
axis equal
view(90,0)
grid minor
print -depsc2 -painters Plots_Dumbbell/Dziuk_Dumbbell_h=0.1_solution_at_t0_BDF2_tau0.003.eps


%t2=nexttile(4);
% t2=subplot(4,3,4)
load("Result_MCF_Dziuk/Dumbbell_h=0.1_solution_at_t0.051_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(90,0)
 zlabel('Zeitpunkt 0.051');
grid minor

print -depsc2 -painters Plots_Dumbbell/Dziuk_Dumbbell_h=0.1_solution_at_t0.051_BDF2_tau0.003.eps

%t3=nexttile(7);
% t3=subplot(4,3,7)
load("Result_MCF_Dziuk/Dumbbell_h=0.1_solution_at_t0.063_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(90,0)
 zlabel('Zeitpunkt 0.063');
grid minor
print -depsc2 -painters Plots_Dumbbell/Dziuk_Dumbbell_h=0.1_solution_at_t0.063_BDF2_tau0.003.eps

% t4= nexttile(10);
% t4=subplot(4,3,10)
load("Result_MCF_Dziuk/Dumbbell_h=0.1_solution_at_t0.078_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(90,0)
 zlabel('Zeitpunkt 0.078');
grid minor
print -depsc2 -painters Plots_Dumbbell/Dziuk_Dumbbell_h=0.1_solution_at_t0.078_BDF2_tau0.003.eps

%% Dziuk ALE plots
%t1=subplot(4,3,1)
load("Result_MCF_Dziuk_ALE/Dumbbell_h=0.1_solution_at_t0_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
 title('Dziuk ALE')
% zlabel('Zeitpunkt 0');
axis equal
view(90,0)
grid minor
print -depsc2 -painters Plots_Dumbbell/Dziuk_ALE_Dumbbell_h=0.1_solution_at_t0_BDF2_tau0.003.eps


%t2=nexttile(4);
% t2=subplot(4,3,4)
load("Result_MCF_Dziuk_ALE/Dumbbell_h=0.1_solution_at_t0.051_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(90,0)
%  zlabel('Zeitpunkt 0.051');
grid minor

print -depsc2 -painters Plots_Dumbbell/Dziuk_ALE_Dumbbell_h=0.1_solution_at_t0.051_BDF2_tau0.003.eps

%t3=nexttile(7);
% t3=subplot(4,3,7)
load("Result_MCF_Dziuk_ALE/Dumbbell_h=0.1_solution_at_t0.063_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(90,0)
%  zlabel('Zeitpunkt 0.063');
grid minor
print -depsc2 -painters Plots_Dumbbell/Dziuk_ALE_Dumbbell_h=0.1_solution_at_t0.063_BDF2_tau0.003.eps

% t4= nexttile(10);
% t4=subplot(4,3,10)
load("Result_MCF_Dziuk_ALE/Dumbbell_h=0.1_solution_at_t0.078_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(90,0)
%  zlabel('Zeitpunkt 0.078');
grid minor
print -depsc2 -painters Plots_Dumbbell/Dziuk_ALE_Dumbbell_h=0.1_solution_at_t0.078_BDF2_tau0.003.eps



%% Lubich plot
% nexttile(2)

% subplot(4,3,2)
load("Result_MCF_Lubich/Dumbbell_h=0.1_solution_at_t0_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
title('Kov�cs et al')
axis equal
view(90,0)
grid minor
print -depsc2 -painters Plots_Dumbbell/Kovacs_Dumbbell_h=0.1_solution_at_t0_BDF2_tau0.003.eps


% nexttile(5)
% subplot(4,3,5)
load("Result_MCF_Lubich/Dumbbell_h=0.1_solution_at_t0.051_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(90,0)
grid minor

print -depsc2 -painters Plots_Dumbbell/Kovacs_Dumbbell_h=0.1_solution_at_t0.051_BDF2_tau0.003.eps

% nexttile(8)
% subplot(4,3,8)
load("Result_MCF_Lubich/Dumbbell_h=0.1_solution_at_t0.063_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(90,0)
grid minor

print -depsc2 -painters Plots_Dumbbell/Kovacs_Dumbbell_h=0.1_solution_at_t0.063_BDF2_tau0.003.eps


% nexttile(11)
% subplot(4,3,11)
load("Result_MCF_Lubich/Dumbbell_h=0.1_solution_at_t0.078_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(90,0)
grid minor

print -depsc2 -painters Plots_Dumbbell/Kovacs_Dumbbell_h=0.1_solution_at_t0.078_BDF2_tau0.003.eps

%% Lubich ALE
% nexttile(3)
% subplot(4,3,3)
load("Result_MCF_Lubich_ALE/Dumbbell_h=0.1_solution_at_t0_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
title('Kov�cs et al. ALE')
axis equal
view(90,0)
grid minor

print -depsc2 -painters Plots_Dumbbell/Kovacs_ALE_Dumbbell_h=0.1_solution_at_t0_BDF2_tau0.003.eps

% nexttile(6)
% subplot(4,3,6)
load("Result_MCF_Lubich_ALE/Dumbbell_h=0.1_solution_at_t0.051_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(90,0)
grid minor
print -depsc2 -painters Plots_Dumbbell/Kovacs_ALE_Dumbbell_h=0.1_solution_at_t0.051_BDF2_tau0.003.eps


% nexttile(9)
% subplot(4,3,9)
load("Result_MCF_Lubich_ALE/Dumbbell_h=0.1_solution_at_t0.063_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(90,0)
grid minor
print -depsc2 -painters Plots_Dumbbell/Kovacs_ALE_Dumbbell_h=0.1_solution_at_t0.063_BDF2_tau0.003.eps


% nexttile(12)
% subplot(4,3,12)
load("Result_MCF_Lubich_ALE/Dumbbell_h=0.1_solution_at_t0.078_BDF2_tau0.003.mat")
trisurf(elements,x_neu(:,1),x_neu(:,2),x_neu(:,3))
axis equal
view(90,0)
grid minor

print -depsc2 -painters Plots_Dumbbell/Kovacs_ALE_Dumbbell_h=0.1_solution_at_t0.078_BDF2_tau0.003.eps

